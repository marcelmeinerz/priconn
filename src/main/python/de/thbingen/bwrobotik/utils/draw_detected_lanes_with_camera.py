import numpy as np
import cv2
import argparse
import imutils
import time
import sys


sys.path.append('../priconn_system')
from scipy.misc import imresize

from keras.models import load_model
from Lanes import Lanes


# construct the argument parse and parse the arguments
ap = argparse.ArgumentParser()

ap.add_argument("-w", "--width", type=int, default=227)
ap.add_argument("-h", "--height", type=int, default=227)
ap.add_argument("-i", "--input", default='../priconn_system/init_weights/init_weights.h5', help="path to input h5 file")
ap.add_argument("-p", "--picamera", type=int, default=0, help="whether or not the Raspberry Pi camera should be used")

args = vars(ap.parse_args())

# Load Keras model
model = load_model(args["input"])

# Class to average lanes with

lanes = Lanes(model, (args["width"], args["height"]))

# initialize the video stream and allow the camera
# sensor to warmup
print("[INFO] warming up camera...")

vs = cv2.VideoCapture(args["picamera"])
time.sleep(2.0)

# loop over frames from the video stream

try:
    print("[INFO] start streaming...")
    while True:
        # grab the frame from the video stream and resize it to have a
        # maximum width of 300 pixels
        ret, frame = vs.read()
        frame = imutils.resize(frame, width=720, height=540)
        cv2.imshow("Frame", frame)

        frame = lanes.predict_fast(frame)

        # show the frames
        cv2.imshow("Output", frame)
        cv2.waitKey(1) & 0xFF


except KeyboardInterrupt:

    pass

finally:

    print("[INFO] cleaning up...")

    cv2.destroyAllWindows()

    vs.release()
