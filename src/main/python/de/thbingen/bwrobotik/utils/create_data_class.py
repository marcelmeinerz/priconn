from PIL import Image
from numpy import array
from tqdm import tqdm
import pickle
import numpy as np
import glob
import random
import cv2
import os
import re
import copy
import argparse
import logging
import json
import uuid

import sys
sys.path.append('../priconn_system/train')
from Utils import *


ap = argparse.ArgumentParser()
ap.add_argument("-d", "--data", required=True, help="path to data json file")
args = vars(ap.parse_args())


class Data:

    def __init__(self, data):

        self.data = json.load(open(data, "r"))
        logging.basicConfig(format='%(asctime)s %(levelname)s %(message)s',
                            level=logging.INFO,
                            stream=sys.stdout)
        self.param = self.data[PARAM]
        self.save_path = self.param[SAVE]
        self.augmented = self.param[AUG]
        self.size = (self.param[WIDTH], self.param[HEIGHT])
        self.package = self.param[PACKAGE]
        self.current_dataset = None
        self.input_image_path = self.param[IMAGE_TO_ADD]
        self.ratio = self.input_image_path[RATIO]


    def __check_shape(self, label, image):

        rows1, cols1, channel1 = image.shape
        rows2, cols2, channel2 = label.shape

        if not (rows1 == rows2 and cols1 == cols2):
            label = cv2.resize(label, (cols1, rows1))
            logging.info("Change shape from " + str((rows2, cols2, channel2)) + " to " + str(label.shape) + ".")

        return label

    def __paint_object(self, image, pt, isLabel):

        try:

            img = image.copy()

            if isLabel:
                obj = cv2.imread(self.input_image_path[L_PATH])
            else:
                obj = cv2.imread(self.input_image_path[I_PATH])

            obj = cv2.resize(obj, ((int)(image.shape[1]*self.ratio), (int)(image.shape[0]*self.ratio)))

            rows, cols, ch = obj.shape

            roi = img[pt[0][0]:pt[1][0], pt[0][1]:pt[1][1]]

            # Now create a mask of logo and create its inverse mask also
            objgray = cv2.cvtColor(obj,cv2.COLOR_BGR2GRAY)
            ret, mask = cv2.threshold(objgray, 10, 255, cv2.THRESH_BINARY)

            mask_inv = cv2.bitwise_not(mask)

            # Now black-out the area of logo in ROI
            img_bg = cv2.bitwise_and(roi, roi, mask=mask_inv)

            # Take only region of logo from logo image.
            obj_fg = cv2.bitwise_and(obj,obj,mask = mask)

            # Put logo in ROI and modify the main image
            dst = cv2.add(img_bg,obj_fg)
            img[pt[0][0]:pt[0][0]+rows , pt[0][1]:pt[0][1]+cols] = dst
    
        except:

            logging.error(sys.exc_info())
            logging.error("Image with shape " + str(image.shape) + " has an error with roi " + str(roi.shape) + " and object shape " + str(obj.shape) + "!")
            exit(1)

        return img


    def __sp_noise(self, image, prob, scaling_width, scaling_height):

        output_vertex = []

        thres = 1 - prob

        # Iterate for each pixel to find random position for pylon in bottom half of image
        for i in range((int)(image.shape[1] / 4), (int)(image.shape[1] / 4) * 3):
            for j in range((int)(image.shape[0] / 2), image.shape[0]):

                rdn = random.random()

                if rdn < prob and len(output_vertex) < 2:

                    top = i
                    bottom = i + scaling_height
                    left = j
                    right = j + scaling_width

                    # If and only if the possible pylone fits in image, the random pixel will add to list
                    if (scaling_width < i) and (i + scaling_height < image.shape[1]) and (scaling_height < j) and (
                            j + scaling_width < image.shape[0]):
                        output_vertex.append(((left, top), (right, bottom)))

        return output_vertex


    def __crop_image(self, image, channel):

        shp = image.shape
        x2 = (int)(shp[0] / 2)
        y2 = (int)(shp[1] / 2)

        crop_image = [image[0:x2, 0:y2], image[0:x2, y2:shp[1]], image[x2:shp[1], 0:y2], image[x2:shp[0], y2:shp[1]]]

        crop_image_refactor = []

        for img in crop_image:
            img = cv2.resize(img, self.size)
            img = img.reshape(img.shape[0], img.shape[1], channel)
            crop_image_refactor.append(img)

        return crop_image_refactor


    def __gry_scale(self, image):

        label = image.copy()

        # Change gt definitions to gt color of fcn
        label[np.where((label == self.current_dataset[BACKGROUND]).all(axis=2))] = BACKGROUND_COLOR
        label[np.where((label == self.current_dataset[ROAD]).all(axis=2))] = ROAD_COLOR
        label[np.where((label == self.input_image_path[IMAGE_TO_ADD_COLOR]).all(axis=2))] = BACKGROUND_COLOR

        label = cv2.cvtColor(label, cv2.COLOR_BGR2GRAY)
        return label

    def __create_augmented_data(self, image, label):

        current_x_data = []

        current_y_data = []

        gray_label = self.__gry_scale(label)

        # Flipping image
        image_flip = cv2.flip(image, 1)
        image_flip = cv2.resize(image_flip, self.size)
        current_x_data.append(image_flip)

        # Crooping image
        current_x_data.extend(self.__crop_image(image, 3))

        # Flipping groundtruth
        label_flip = cv2.flip(gray_label, 1)
        label_flip = cv2.resize(label_flip, self.size)
        label_flip = label_flip.reshape(image_flip.shape[0], image_flip.shape[1], 1)
        current_y_data.append(label_flip)

        # Crooping groundtruth
        current_y_data.extend(self.__crop_image(gray_label, 1))

        # Generate pylone for image & gt image
        vertex = self.__sp_noise(image, PROPABILITY, (int)(image.shape[0] * self.ratio),
                                 (int)(image.shape[1] * self.ratio))

        for pt in vertex:
            image = self.__paint_object(image, pt, False)
            label = self.__paint_object(label, pt, True)

        image = cv2.resize(image, self.size)
        label = self.__gry_scale(label)
        label = cv2.resize(label, self.size)
        label = label.reshape(image.shape[0], image.shape[1], 1)
        current_x_data.append(image)
        current_y_data.append(label)

        return (current_x_data, current_y_data)


    def __mkdir(self, r_var,  as_dump=False):

        if as_dump:

            self.save_folder_train = self.save_path+DUMP_FOLDER+TRAIN_FOLDER
            self.save_folder_label = self.save_path+DUMP_FOLDER+LABEL_FOLDER

        else:

            self.save_folder_train = self.save_path+FILE_FOLDER+TRAIN_FOLDER
            self.save_folder_label = self.save_path+FILE_FOLDER+LABEL_FOLDER


        if not os.path.exists(self.save_folder_train):
            os.makedirs(self.save_folder_train)
        if not os.path.exists(self.save_folder_label):
            os.makedirs(self.save_folder_label)

        # Set folders to save image dump files and label dump files
        train_folder = self.save_folder_train
        label_folder = self.save_folder_label

        # If and only if shuffle is set, the folders will be parted to given size

        if self.package[SHUFFLE]:

            train_folder = train_folder + SHUFFLE_FOLDER + str(r_var)
            if not os.path.exists(train_folder):
                os.makedirs(train_folder)

            label_folder = label_folder + SHUFFLE_FOLDER + str(r_var)
            if not os.path.exists(label_folder):
                os.makedirs(label_folder)

        return (train_folder, label_folder)


    def __save_dump_file(self, current_x_data, current_y_data, r_var):
        
        #Set folders to save image dump files and label dump files
        train_folder, label_folder = self.__mkdir(r_var, True)


        # Save the given list as pickle dump file
        print('Current_X_data shape:', np.array(current_x_data).shape)
        with open(train_folder+'/input_image_'+self.current_dataset[SUFFIX]+str(r_var)+'.p', 'wb') as save_file:
            pickle.dump(np.array(current_x_data), save_file, -1)
        print('Current_Y_data shape:', np.array(current_y_data).shape)
        with open(label_folder+'/gt_image_'+self.current_dataset[SUFFIX]+str(r_var)+'.p', 'wb') as save_file:
            pickle.dump(np.array(current_y_data), save_file, -1)


    def __save_file(self, current_x_data, current_y_data, r_var):

        # Set folders to save image dump files and label dump files
        train_folder, label_folder = self.__mkdir(r_var)

        for i in range(len(current_x_data)):

            image_name = uuid.uuid4().hex + ".png"

            while os.path.exists(train_folder+image_name):

                image_name = uuid.uuid4().hex + ".png"

            with open(train_folder + image_name, 'w') as save_file:
                cv2.imwrite(save_file.name, current_x_data[i])
            with open(label_folder + image_name, 'w') as save_file:
                cv2.imwrite(save_file.name, current_y_data[i])


    def __get_image_list(self):

        list_images = [self.current_dataset[I_PATH] + f for f in os.listdir(self.current_dataset[I_PATH]) if
                       re.search('png|jpg', f)]

        return list_images


    def __get_partition(self):

        partition = self.package[SIZE]

        # If shuffle is true, the packages will be sized to ratio of number of images
        if self.package[SHUFFLE]:
            partition = (int)(len(self.__get_image_list()) / self.package[SIZE])

        return partition


    def create_dump_file(self):

        list_images = self.__get_image_list()

        current_x_data = []

        current_y_data = []

        r_var = 0

        partition = self.__get_partition()

        for i in tqdm(range(len(list_images))):

            # Load images and prepare augmentation data for image
            myFile = list_images[i]
            myFile_name = myFile.split('/')[len(myFile.split('/')) - 1]
            image = cv2.imread(myFile, 3)
            image_aug = image.copy()
            image = cv2.resize(image, self.size)
            current_x_data.append(image)

            # Load label and prepare augmentation data for label
            label = cv2.imread(self.current_dataset[L_PATH] + self.current_dataset[DESCR] + myFile_name)
            label = self.__check_shape(label, image_aug)
            label_aug = label.copy()
            label = self.__gry_scale(label)
            label = cv2.resize(label, self.size)
            label = label.reshape(image.shape[0], image.shape[1], 1)
            current_y_data.append(label)

            # If augemented is set, the data augmentation starts
            if self.augmented:
<<<<<<< HEAD

                x_d, y_d = self.__create_augmented_data(image_aug, label_aug)
=======
                x_d, y_d = self.__create_augmented_data(image_aug, label_aug, myFile)
>>>>>>> a9cfe334563981773dcbeb22248515d57f4d575f

                current_x_data.extend(x_d)
                current_y_data.extend(y_d)

            #Save the file to given package size           
            if len(current_x_data) >= int(partition):


                if creator.data[PARAM][ARCHIVE]:

                    self.__save_dump_file(current_x_data, current_y_data, r_var)

                else:

                    self.__save_file(current_x_data, current_y_data, r_var)

                r_var = r_var + 1

                current_x_data = []

                current_y_data = []

        # Final save of dump file
        if creator.data[PARAM][ARCHIVE]:

            self.__save_dump_file(current_x_data, current_y_data, r_var)

        else:

            self.__save_file(current_x_data, current_y_data, r_var)


if __name__ == '__main__':

    creator = Data(args[DATA])

    for creator.current_dataset in creator.data[DATA]:
        creator.create_dump_file()
