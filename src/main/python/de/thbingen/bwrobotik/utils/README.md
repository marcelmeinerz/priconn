# PRICoNN
## Passable Road Identification with a Convolutional Neural Network

## Dataset normalization

If you want to use your dataset, you have to normalize your images. To normalize you can use the scripts of [utils](src/main/python/de/thbingen/bwrobotik/utils/) folder.
To use your dataset you have to run [create_data_class.py](src/main/python/de/thbingen/bwrobotik/utils/create_data_class.py) and change the parameter in [data.json](src/main/python/de/thbingen/bwrobotik/utils/data.json).

`python create_data_class.py --data [path to data json file]`

The data.json has following inserts:

### Parameter:

		"params" : {
			"augmented" : true ,
			"save_path" : [path_to_save],
			"width" : 227 ,
			"height" : 227,
			"package" : {
							"shuffle" : true,
							"size" : 8
			},
			"image_to_add" : {
							"image" : [path_to_label],
							"label" : [path_to_label],
							"ratio" : 0.1
			}
		}

* augmented : True if you want to augment your dataset
* save_path : Path to save the dump files
* width : Width of needed input image
* height : Height needed of input image
* package : Object to configure the save method
    * shuffle : True is you want to shuffle the different datasets
    * size : If shuffle is true you need the number of partitions of dataset, otherwise number of images per dump file
* image_to_add : Object to configure the "Add Image" for the Salt & Pepper augmentation
    * image : Path to the image, which will add to copy of original image
	* label : Path to the label, which will add to copy of original label
 	* ratio : Ratio of image which will been add to images


### Data:

		"data" : [
			{
			"image_dir" : "[path_to_image]" ,
			"label_dir" : "[path_to_label]" ,
			"suffix" : "[unique identifier]" ,
			"description" : "gt_",
			"road_color" : [255, 0, 255] ,
			"background_color" : [255, 0, 0]
			},
			{
				...
			}
		]


* image_dir : Path to image folder
* image_dir : Path to gt_image folder
* suffix : Unique identifier to save the dump file
* description : Description for groundtruth identification
* road_color : Predefined road color of label
* background_color : Predefined background color of label


**Note: With augmentation the dataset will be grow with factor 7.**

`python create_data.py --folder [path to save the dump files] -a`

To check your pickle dump file you can use [create_image_from_p.py](src/main/python/de/thbingen/bwrobotik/utils/create_image_from_p.py) to extract the images and labels.

`python create_image_from_p.py --dump_image [path to image dump files] --dump_label [path to label dump files]`

**Note: [create_image_from_p.py](src/main/python/de/thbingen/bwrobotik/utils/create_image_from_p.py) save all images in `./data_aug/[prefix_image_name]`with size (1280, 640).**
