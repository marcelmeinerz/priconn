# PRICoNN
## Passable Road Identification with a Convolutional Neural Network

The aim of this work is the realization, training and evaluation of a "Fully Convolutional Neural Network" (FCN) for the recognition of roads and paths that can be driven on with colour cameras. The Tensorflow framework is especially discussed and compared with the Python Library keras implemented.

<img src="src/main/resources/show.gif" width="350" height="250">  <img src="src/main/resources/insta.gif" width="350" height="250">

## Tutorial

### Getting started

Run `python start_priconn.py -c configuration/config.json` to start the learning process of this CNN. The previously downloaded inital model will be set automatically.

The following options are available and defined as:

* `-c | --config [path to configuration JSON file]`	: Parameter to set a list of configuration JSON files with their path, is required. [more details](configuration/README.md)
* `-w | --weights [path to .h5 file]`	            : Parameter to use initial weights, not required
* `-m | --model [path to .h5 file]`	                : Parameter to use pretrained model, not required
* `-e | --eval [path to evaluation JSON file]`      : Parameter to use the evaluation during training, not required. [more details](evaluation/README.md)
* `-d | --logdir [log folder]`		                : Parameter to set the folder of log files, default is logs, not required

## Evaluation

Run `python start_evaluation.py -e evaluation/eval.json` to test the pretrained system. The initial model will be load automatically.

If you want to evaluate during the training you can run `python start_priconn.py -c configuration/config.json -e evaluation/eval.json`.

The sample files for evaluation are downloaded at the very begin of, by running `./setup.sh`.

For more details look in [Evaluation](evaluation/README.md)

# Citation

If you benefit from this code, please cite my work:

```
@article{meinerz2018priconn,
  title={PRICoNN: Passable Road Identification with a Convolutional Neural Network},
  author={Meinerz, Marcel},
  year={2018}
  month = {Aug},
  url = {https://gitlab.bwrobotik.selfhost.de/bwrobotik/priconn.git}
}
```
