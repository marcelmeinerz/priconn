import argparse
import os
import re
import sys
import json
import logging, coloredlogs
import shutil


sys.path.append('./evaluation')
from Eval import Eval
sys.path.append('./train')
from Data import Data
from Utils import *
from keras.models import load_model


ap = argparse.ArgumentParser()
ap.add_argument("-e", "--eval", required=True, help="paths to the eval files")
ap.add_argument("-w", "--weights", default='init_model/PRICoNN.h5',
                help="Initial weights for CNN")
args = vars(ap.parse_args())

if __name__ == "__main__":
    coloredlogs.install(level=logging.INFO)
    logging.basicConfig(format='%(asctime)s %(levelname)s %(message)s', level=logging.INFO, stream=sys.stdout)


    f = open(args["eval"], "r")

    cfg = json.load(f)

    eval_o = Eval(cfg, load_model(args["weights"]))

    data_o = Data((cfg[WIDTH], cfg[HEIGHT], cfg[CHANNEL]), cfg[ERROR][BACKGROUND], cfg[ERROR][ROAD])

    eval_o.eval(data_o)

    if os.path.exists(TEMP): 

        shutil.rmtree(TEMP)   


