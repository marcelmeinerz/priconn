
import numpy as np
import pickle
import matplotlib.pyplot as plt
import cv2
import os
import re
import sys
import logging
import itertools
from tqdm import tqdm

from Utils import *

from sklearn.utils import shuffle
from sklearn.model_selection import train_test_split
from sklearn.metrics import confusion_matrix, classification_report

"""Data module

.. moduleauthor:: Marcel Meinerz <marcel.meinerz@th-bingen.de>


"""

class Data:

    """Class Data to load and hold the input and evaluation data
    """

    def __init__(self, input_shape, background, road):
        self.X_train = []
        self.y_train = []
        self.X_val = []
        self.y_val = []

        self.eval_sample = []
        self.eval_label = []

        self.background = background
        self.road = road

        self.input_shape = input_shape

    def get_train_data(self):

        return (self.X_train, self.X_val, self.y_train, self.y_val)

    def get_eval_data(self):

        return (self.eval_sample, self.eval_label)

    def isGT(self):

        return self.background and self.road  

    def setGroundtruth(self, background, road):

        self.background = background
        self.road = road

    def __load_from_file(self, input_image_path, input_label_path):

        """Method __load_from_file() loads the image files from directory.

        :param input_image_path: Path to the input images for training. 
        :type input_image_path: string
        :param input_label_path: Path to groundtrtuh images for compare the prediction. 
        :type input_label_path: string

        :returns: List of images and labels as numpy.array. 
        :return type: list (type numpy.array, type numpy.array)

        """

        logging.info("Load data...")
        
        list_images = [input_image_path+f for f in os.listdir(input_image_path) if re.search('.png|.jpg', f)]
        
        train_images = []
        labels = []

        for img in tqdm(list_images):

            image = cv2.imread(img)
 
            image_name = img.split('/')[len(img.split('/'))-1]

            label = cv2.imread(input_label_path+image_name)

            if image.shape[0] != self.input_shape[0] or image.shape[1] != self.input_shape[1]:

                image = cv2.resize(image, (self.input_shape[1], self.input_shape[0]))
                label = cv2.resize(label, (self.input_shape[1], self.input_shape[0]))


            train_images.append(image)

            #Change gt definitions to gt color of fcn
            label[np.where((label==self.background).all(axis=2))] = BACKGROUND_COLOR
            label[np.where((label==self.road).all(axis=2))] = ROAD_COLOR
            label = cv2.cvtColor(label, cv2.COLOR_BGR2GRAY)
            label = label.reshape(self.input_shape[0], self.input_shape[1], 1)

            labels.append(label)

        return (train_images, labels)

    def __load_from_dump_file(self, input_image_path, input_label_path):

        """Method __load_from_dump_file() loads the image files from pickle archive.

        :param input_image_path: Path to the input images for training. 
        :type input_image_path: string
        :param input_label_path: Path to groundtrtuh images for compare the prediction. 
        :type input_label_path: string

        :returns: List of images and labels as numpy.array.  
        :return type: list (type numpy.array, type numpy.array)			

        """

        logging.info("Load data...")
        if os.path.isfile(input_image_path):
            logging.info("Load single training data...")
            train_images = pickle.load(open(input_image_path, "rb" ))
            logging.info("Load single label data...")
            labels = pickle.load(open(input_label_path, "rb" ))

        else:

            list_images = [input_image_path+f for f in os.listdir(input_image_path) if re.search('.p|.pkl', f)]
            list_label = [input_label_path+f for f in os.listdir(input_label_path) if re.search('.p|.pkl', f)]
            list_images.sort()
            list_label.sort()
            train_images = []
            labels = []

            logging.info("Load training data...")
            # Load training images
            for pfile in list_images:
                print(pfile)
                train_images.extend(pickle.load(open(pfile, "rb" )))

            logging.info("Load lable data...")
            # Load image labels
            for pfile in list_label:
                print(pfile)
                labels.extend(pickle.load(open(pfile, "rb" )))


        logging.info(str(len(train_images)) + " images were load for input with total size of " + str(int((np.asarray(train_images).nbytes / 1024**3) + (np.asarray(labels).nbytes / 1024**3))) + " GByte.")
        

        return (train_images, labels)

    def load_eval_data(self, input_image_path, input_label_path, archive):

        """Method load_eval_data() loads the given image files from JSON configuration.	

        :param input_image_path: Path to the input images for training. 
        :type input_image_path: string
        :param input_label_path: Path to groundtrtuh images for compare the prediction. 
        :type input_label_path: string
        :param archive: rue if the file is an pickle archive, False if the file is an image type. 
        :type archive: bool

        :returns: List of normalized images as numpy.array for evaluation.  
        :return type: list (type numpy.array, type numpy.array)		

        """

        if archive:

            self.eval_sample, self.eval_label = self.__load_from_dump_file(input_image_path, input_label_path)

        else:

            self.eval_sample, self.eval_label = self.__load_from_file(input_image_path, input_label_path)

        # Make into arrays as the neural network wants these
        self.eval_sample = np.array(self.eval_sample)
        self.eval_label = np.array(self.eval_label)

        # Normalize labels - training images get normalized to start in the network
        self.eval_label = self.eval_label / 255

        return self.get_eval_data()


    def load_train_data(self, input_image_path, input_label_path, archive, is_shuffle):

        """Method load_train_data() load the given image files from JSON configuration.	

        :param input_image_path: Path to the input images for training. 
        :type input_image_path: string
        :param input_label_path: Path to groundtrtuh images for compare the prediction. 
        :type input_label_path: string
        :param archive: rue if the file is an pickle archive, False if the file is an image type. 
        :type archive: bool

        :returns: List of normalized images as numpy.array for training and evaluation.  
        :return type: list (type numpy.array, type numpy.array, type numpy.array, type numpy.array)		

        """

        train_images = []
        labels = []

        if archive:

            train_images, labels = self.__load_from_dump_file(input_image_path, input_label_path)

        else:

            train_images, labels = self.__load_from_file(input_image_path, input_label_path)

        # Make into arrays as the neural network wants these
        train_images = np.array(train_images)
        labels = np.array(labels)

        # Normalize labels - training images get normalized to start in the network
        labels = labels / 255

        # Shuffle images along with their labels, then split into training/validation sets
        if is_shuffle:

            logging.info("Shuffle data...")
            train_images, labels = shuffle(train_images, labels)

        # Test size may be 10% or 20%
        self.X_train, self.X_val, self.y_train, self.y_val = train_test_split(train_images, labels, test_size=0.2)

        logging.info("Train on " + str(len(self.X_train)) + " images, eval on " + str(len(self.X_val)) + " images.")

        return self.get_train_data()

