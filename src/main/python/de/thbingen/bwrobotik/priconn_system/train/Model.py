"""Model module

.. moduleauthor:: Marcel Meinerz <marcel.meinerz@th-bingen.de>


"""

import numpy as np

from Utils import *
# Import necessary items from Keras
from keras.models import Sequential, clone_model
from keras.layers import Activation, Dropout, UpSampling2D, LeakyReLU
from keras.layers import Conv2DTranspose, Conv2D, MaxPooling2D
from keras.layers.normalization import BatchNormalization

class Singleton(object):

    def __new__(cls, *args, **kwds):

        it = cls.__dict__.get("__it__")

        if it is not None:

            return it

        cls.__it__ = it = object.__new__(cls)

        it.init(*args, **kwds)

        return it

    def __init__(self, *args, **kwds):
        pass


class Model(Singleton):

    """Class Model to initialize the neural network as Sequential object..

    """
    def init(self, activation, input_shape):

        self.model = Sequential()        
        self.activationType = activation
        self.input_shape = input_shape
        self.__create_model()

    def __activation(self, activationType):

        """
        Method to create the activation function of the neural network.

        :param activationType: Name of keras activation types. 
        :type activationType: string

        :returns: Object of class Activation. 
        :return type: keras.layers.Activation

        """

        if activationType == 'LeakyReLU':

            return LeakyReLU(alpha = 1e-20)

        elif activationType in ACTIVATION_TYPES:

            return Activation(activationType)

        else:

            return Activation('relu')

    def __create_model(self):

        """
        Method to create the model.

        :returns: Object of class Sequential. 
        :return type: keras.models.Sequential

        """

        # Batch size, epochs and pool size below are all paramaters to fiddle with for optimization

        input_padding = 'same'

        # Normalizes incoming inputs. First layer needs the input shape to work
        self.model.add(BatchNormalization(input_shape = self.input_shape))
        #############################Conv_1####################################################

        self.model.add(Conv2D(96, (11, 11), padding='valid', strides=(4, 4), name = 'Conv1'))

        self.model.add(self.__activation(self.activationType))

        self.model.add(MaxPooling2D(pool_size=(3, 3), strides=(2, 2)))

        self.model.add(BatchNormalization())

        #############################Conv_2####################################################

        self.model.add(Conv2D(256, (5, 5), padding=input_padding, strides=(1, 1), name = 'Conv2'))

        self.model.add(self.__activation(self.activationType))

        self.model.add(MaxPooling2D(pool_size=(3, 3), strides=(2, 2)))

        self.model.add(BatchNormalization())

        #############################Conv_3-5####################################################

        self.model.add(Conv2D(384, (3, 3), padding=input_padding, strides=(1, 1), name = 'Conv3_0'))

        self.model.add(self.__activation(self.activationType))

        self.model.add(Conv2D(384, (3, 3), padding=input_padding, strides=(1, 1), name = 'Conv3_1'))

        self.model.add(self.__activation(self.activationType))

        self.model.add(Conv2D(256, (3, 3), padding=input_padding, strides=(1, 1), name = 'Conv3_2'))

        self.model.add(self.__activation(self.activationType))

        self.model.add(MaxPooling2D(pool_size=(3, 3), strides=(2, 2)))

        ##############################Conv_4###################################################

        self.model.add(Conv2D(4096, (1, 1), padding=input_padding, strides=(1, 1),name = 'Conv_4_0'))

        self.model.add(self.__activation(self.activationType))

        self.model.add(Dropout(0.5))

        self.model.add(Conv2D(4096, (1, 1), padding=input_padding, strides=(1, 1),name = 'Conv_4_1'))

        self.model.add(self.__activation(self.activationType))

        self.model.add(Dropout(0.5))

        ##############################DeConv_1#################################################

        self.model.add(UpSampling2D((2, 2)))

        self.model.add(Conv2DTranspose(384 , (3, 3), padding=input_padding, strides=(1, 1),  name = 'DeConv3_2'))

        self.model.add(self.__activation(self.activationType))

        ##############################DeConv_2#################################################

        self.model.add(UpSampling2D((2, 2)))

        self.model.add(Conv2DTranspose(256 , (5, 5), padding=input_padding, strides=(1, 1),  name = 'DeConv2'))

        self.model.add(self.__activation(self.activationType))

        ##############################DeConv_3#################################################

        self.model.add(UpSampling2D((2, 2)))

        self.model.add(Conv2DTranspose(96 , (11, 11), padding='valid', strides=(4, 4),  name = 'DeConv1'))

        self.model.add(self.__activation(self.activationType))

        ##############################Final####################################################      

        self.model.add(Conv2DTranspose(1 , (29, 29), padding='valid', strides=(1, 1),  name = 'Final'))

        self.model.add(self.__activation(self.activationType))

        return self.model

    def clone(self):

        return clone_model(self.model)

    def get_model(self):

        return self.model

    def load_weights(self,weights):

        self.model.load_weights(weights, True)



