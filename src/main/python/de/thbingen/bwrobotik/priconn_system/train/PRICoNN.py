"""PRICoNN module

.. moduleauthor:: Marcel Meinerz <marcel.meinerz@th-bingen.de>


"""

import numpy as np
import matplotlib.pyplot as plt
import tensorflow as tf
import time
import datetime
import os
import re
import sys
import logging, coloredlogs
import gc
import json
import copy
import h5py

from Data import Data
from Model import Model
from Utils import *
from Optimizer import Optimizer_Factory

sys.path.append('./evaluation')
from Eval import Eval

from shutil import copyfile

# Import necessary items from Keras
from keras.preprocessing.image import ImageDataGenerator
from keras.callbacks import History, TensorBoard, CSVLogger, EarlyStopping, ModelCheckpoint
from keras import backend as K
from keras.models import load_model, save_model


CONSOLE = '/console.log'
BEST_OF = '/best_of.txt'
CURRENT = 'temp_model.h5'

class PRICoNN:
   
    """Class PRICoNN for train and evaluate a given set of input images on individual configuration from JSON file.

    """
    def __init__(self, config, logdir, weights, model, eval_file):
        self.logdir_path = logdir
        self.config_path = config
        self.model = model
        self.weights_path = weights
        self.eval_file = eval_file
        self.eval = None
        self.runner = 0
        
    def __init_components(self):

        #Open JSON config file and set json object

        f = open(self.config_path, "r")
        self.configs = json.load(f)

        #Open JSON evaluation file and set json object
        if self.eval_file is not None:

            f = open(self.eval_file, "r")
            self.eval = json.load(f)

        #Set input shape for training and evaluation data
        self.input_shape = (self.configs[PARAM][HEIGHT],self.configs[PARAM][WIDTH],self.configs[PARAM][CHANNEL])
  
        #Init of optimizer factory for given settings in config file
        self.factory_o = Optimizer_Factory(self.configs[OPTIMIZER])

        #Create workspace folder for results and log data
        std = datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d_%H:%M')

        self.main_folder = self.configs[PARAM][SAVE]+str(std)

        if not os.path.exists(self.main_folder):
            os.makedirs(self.main_folder)

        if not os.path.exists(self.main_folder+"/"+self.logdir_path):
            os.makedirs(self.main_folder+"/"+self.logdir_path)

        copyfile(self.config_path, self.main_folder+"/config.json")

        if not os.path.exists(self.config_path):
            self.error.info("You need a config json file. Please enter --config [path_to_config]")
            exit(1)

        if not os.path.exists(TEMP):
            os.makedirs(TEMP)

        #Init logging class for console and .log file
        coloredlogs.install(level=logging.INFO)
        logging.basicConfig(format='%(asctime)s %(levelname)s %(message)s',
                        level=logging.INFO,
                        stream=sys.stdout)

        self.fileHandler = logging.FileHandler(self.main_folder+"/"+self.logdir_path+CONSOLE)
        self.fileHandler.setFormatter(logging.Formatter('%(asctime)s %(levelname)s %(message)s'))

        logging.getLogger('').addHandler(self.fileHandler)

        #Init some variables
        self.index = 0

        self.best_value = 0

    def __init_model(self, model=None, weights=None):

        K.clear_session()

        if model is None:

            #Init foundation model singleton for training
            model_o = Model(self.configs[PARAM][ACTIVATION], self.input_shape)

            model = model_o.clone()

            #Load initial weights for model
            if weights is not None and os.path.exists(weights):

                logging.info("Loaded by weights from "+weights+" ...")
                model.load_weights(weights, True)
                logging.info("Done")

            else:

                logging.warning("You should load the model with initial weights! Without weights the model could prone errors!")

        else:
            logging.info("Load Model ...")
            model = load_model(model)

        return model

    def __init_optimizer(self, model, opt, loss, epochs):
        #Configuration of Optimizer
        opt_callbacks =[]

        if model.optimizer is not None:
            opt_callbacks = self.factory_o.get_optimizer_callbacks(model.optimizer, epochs)
        
        else:
            logging.info("Compile the model with given configurations.")
            optimizer, opt_callbacks = self.factory_o.create_optimizer(opt, epochs)
            model.compile(optimizer=optimizer, loss=loss, metrics= [ACC])

        return opt_callbacks

    def __run_training(self, model,X_train, X_val, y_train, y_val, outFile, batch_size, epochs, other_callbacks, suffix):

        """Method __run_training() starts the training of given model and inividual configurations. This method creates all 
    	Callbacks objects for training control. The Callbacks are from keras library. 
        The method use the following Callbacks:

    	* **History**:			Save the operational sequence
    	* **CSVLogger**: 		Save the results of each sequence
    	* **EarlyStopping**:		Stop the current run if no changes on given parameters
    	* **TensorBoard**:		Use for visualize the current training
        * **ReduceLROnPlateau**:   	Dynamic reduce of learning rate
    	* **LearningRateScheduler**:	Static reduce of learning rate
    	* **ModelCheckpoint**:      	Save checkpoints of training results

        :param model: Model. 
        :type model: keras.model.Sequence
        :param X_train: Input image files for training. 
        :type X_train: numpy.array
        :param X_val: Input image files for evaluation. 
        :type X_val: numpy.array
        :param y_train: Groundtruth image files for compare the output after training. 
        :type y_train: numpy.array
        :param y_val: Groundtruth image files for compare the output after evaluation. 
        :type y_val: numpy.array
        :param outFile: Textfile for logging data. 
        :type outFile: file
        :param opt: Given optimizer for training from json config file. 
        :type opt: string
        :param batch_size: Batch size for training epochs. 
        :type batch_size: int
        :param epochs: Number of epochs for training. 
        :type epochs: int
        :param loss: Given loss function for training from json config file. 
        :type loss: string

        :returns: List of the trained model and it's history. 
        :return type: list (type keras.model, type keras.callbacks.History)

        """

        callbacks = [History()]
        callbacks.extend(other_callbacks)

        # Using a generator to help the model use less data
        # Channel shifts help with shadows slightly
        datagen_o = ImageDataGenerator(channel_shift_range=0.2)        
        datagen_o.fit(X_train)

        #Configuration of Callbacks
        csv_logger_o = CSVLogger(self.main_folder+'/training_'+suffix+'.log')

        callbacks.append(csv_logger_o)

        if self.configs[PARAM][STOP]:

            early_stopper_o = EarlyStopping(monitor=VAL_LOSS, min_delta=0, patience=5, verbose=1, mode='auto')

            callbacks.append(early_stopper_o)
   
        tens_o = TensorBoard(log_dir=self.main_folder+"/"+self.logdir_path+'/logs'+suffix,  batch_size=batch_size, write_graph=True, write_grads=False, write_images=False, embeddings_freq=0, embeddings_layer_names=None, embeddings_metadata=None)
        tens_o.set_model(model)
 
        callbacks.append(tens_o)
        
        # Compiling and training the model
        logging.info("Run training...")

        if not self.configs[PARAM][CHECKPOINT][ONE_4_ALL]:

            self.weights_path = self.main_folder+"/PRICoNN_"+suffix+'.h5'

        else:

            self.weights_path = self.main_folder+"/PRICoNN.h5"

        if self.configs[PARAM][CHECKPOINT][SBO]:

            checkpoint_o = ModelCheckpoint(self.weights_path, monitor=VAL_ACC, verbose=1, save_best_only=True, mode='max')

            if self.configs[PARAM][CHECKPOINT][ONE_4_ALL]:

                checkpoint_o.best = float(self.best_value)              

            callbacks.append(checkpoint_o)

        history_callback = model.fit_generator(datagen_o.flow(X_train, y_train, batch_size=batch_size), steps_per_epoch=len(X_train)/batch_size , epochs=epochs, verbose=1, validation_data=(X_val, y_val), callbacks=callbacks)

        outFile.write(str(history_callback.history))  

        model.save(TEMP+CURRENT)

        if not self.configs[PARAM][CHECKPOINT][SBO]:

            model.save(self.weights_path)

        return history_callback.history


    def train(self):

        """Method train() starts the process to train the model from Model.py with the user defined configuration.
    	Each configuration will be iterate with it's specific information. At the beginning the initial weights
        will be load to the model. After each iteration the trained weights of the previous training will be load
  	to model. For each iteration there will be written an information dataset with results and evaluations.
        The information is in form of a Tensorboard folder, a log folder with all results of each epoch, an evaluation 

        :returns: Path of trained weight of given model. 
        :return type: string
 
        """

        try:

            self.__init_components()

            current_model = self.__init_model(model=self.model, weights=self.weights_path)

            current_model.summary()

            best_history = ""

            #Load dataset
            if not self.configs[PARAM][INPUT][ARCHIVE]:

                assert len(self.configs[PARAM][INPUT][BACKGROUND]) == 3, "For the configuration with no input type archive, you have to set the groundtruth color of the labeld data."
                assert len(self.configs[PARAM][INPUT][ROAD]) == 3, "For the configuration with no input type archive, you have to set the groundtruth color of the labeld data."

            data_o = Data(self.input_shape, self.configs[PARAM][INPUT][BACKGROUND], self.configs[PARAM][INPUT][ROAD])

            for self.index in range(len(self.configs[CONFIG])):

                outFile = open(self.main_folder+"/"+self.logdir_path+"/log_history_"+str(self.index)+".txt", "w")

                try:
                    #Load current configuration for this step
                    cfg = self.configs[CONFIG][self.index]

                    logging.info("This model needs a total memory of "+ str(get_model_memory_usage(cfg[BATCH], current_model)) + " GB.")

                    cbs = self.__init_optimizer(current_model, cfg[OPTIMIZER], cfg[LOSS], cfg[EPOCH])
                                        
                    outFile.write(str(cfg))

                    #Load data for training and evaluation durring the training
                    X_train, X_val, y_train, y_val = data_o.load_train_data(cfg[I_PATH], cfg[L_PATH], self.configs[PARAM][INPUT][ARCHIVE], self.configs[PARAM][INPUT][SHUFFLE])

                    iterations = int(cfg[EPOCH]/self.configs[PARAM][ITERATION])

                    for i in range(iterations):

                        suffix = str(self.index) + '_' +str(i)

                        #Start the training
                        history = self.__run_training(current_model, X_train, X_val, y_train, y_val, outFile , cfg[BATCH], self.configs[PARAM][ITERATION], cbs, suffix)

                        if self.best_value < max(history[VAL_ACC]):

                            self.best_value = max(history[VAL_ACC])

                            best_history = history2String(suffix,history, cfg)
                  
                        #Start the evaluation of current result
                        if self.eval is not None: 

                            eval_o = Eval(self.eval, current_model)    

                            eval_save_path = self.main_folder+"/eval/eval_"+suffix

                            eval_o.eval(data_o, eval_save_path)

                            del eval_o

                        logging.info("Finish part number "+str(i + 1) + " of " + str(iterations))

                        current_model = self.__init_model(model=TEMP+CURRENT)

                    #Start the evaluation of best result
                    if self.eval is not None: 

                        eval_o = Eval(self.eval, self.__init_model(model=self.weights_path))    

                        eval_save_path = self.main_folder+"/eval/eval_best_"+str(self.index)

                        eval_o.eval(data_o, eval_save_path)

                        del eval_o
                
                except:

                    outFile.write(str("\nError occured:"+ str(sys.exc_info()[0])+ str(sys.exc_info()[1])))
                    logging.error(sys.exc_info())

                finally:

                    del current_model

                    gc.collect()

                    current_model = self.__init_model(model=TEMP+CURRENT)

                    outFile.close()

                    logging.info("Finish run number "+str(self.index + 1)+" of "+str(len(self.configs[CONFIG])))

            
            hall_of_fame = open(self.main_folder+"/"+self.logdir_path+BEST_OF, "w")


        finally:

            hall_of_fame.close()

            del data_o
            del current_model

            logging.info("Finish") 

            logging.getLogger('').removeHandler(self.fileHandler)

            return self.weights_path

