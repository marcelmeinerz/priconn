import itertools
import sys
import numpy as np

"""Utils module

.. moduleauthor:: Marcel Meinerz <marcel.meinerz@th-bingen.de>


"""

###PRICONN###
VAL_ACC = 'val_acc'
VAL_LOSS = 'val_loss'
MAE = 'mean_absolute_error'
VAL_MAE = 'val_mean_absolute_error'
LOSS = 'loss'
ACC = 'acc'

###PARAMETER###
PARAM = 'param'
WIDTH = 'width'
HEIGHT = 'height'
CHANNEL = 'channel'
DATA = 'data'
SAVE = 'save_path'
EVAL = 'eval'
STOP = 'early_stopping'
ARCHIVE = 'archive'
ERROR = 'eval_map'
ROAD = 'road_color_bgr'
BACKGROUND = 'background_color_bgr'
IMAGE_TO_ADD_COLOR = 'label_color_bgr'
MAX_VALUE = 'number_of_image'
RESOURCE = 'resource'
ONE_4_ALL = 'one_4_all'
IS_EVAL = 'with_evaluation'
CHECKPOINT = 'checkpoint'
SBO = 'only_the_best'
INPUT = 'input_type'
ITERATION = 'eval_iteration'

###CONFIGURATION###
CONFIG = 'config'
ACTIVATION = 'activation'
OPTIMIZER = 'optimizer'
BATCH = 'batch_size'
EPOCH = 'epochs'
I_PATH = 'path_to_image'
L_PATH = 'path_to_label'
V_PATH = 'video_files'
CM = 'confusion_matrix'

###OPTIMIZER###
DROP = 'drop_rate'
D_RATE = 'decay_rate'
REDUCE = 'reduce_lr'
MONITOR = 'reduce_monitor'
FACTOR = 'reduce_factor'
PATIENCE = 'reduce_patience'
MIN_LR = 'reduce_min_lr'
VERBOSE = 'verbose'
MOM = 'momentum'
LR = 'lr'
BETA_1 = 'beta_1'
BETA_2 = 'beta_2'
EPSILON = 'epsilon'
AMSGRAD = 'amsgrad'

###UTILS###
TRAIN_FOLDER = 'input_image/'
LABEL_FOLDER = 'gt_image/'
FILE_FOLDER = 'as_file/'
DUMP_FOLDER = 'as_dump/'
TEST_FOLDER = 'test/'
ROAD_COLOR = [255,255,255]
BACKGROUND_COLOR = [0,0,0]
IMAGE_TYPE =  ['image', 'flip', 'crop_1', 'crop_2', 'crop_3', 'crop_4', 'pyl']
PROPABILITY = 1e-5
SHUFFLE_FOLDER = "/Custom_"
DESCR = 'description'
SHUFFLE = 'shuffle'
SUFFIX = 'suffix'
SIZE = 'size'
IMAGE_TO_ADD = 'image_to_add'
RATIO = 'ratio'
AUG = 'augmented'
PACKAGE = 'package'
TEMP = 'tmp/'


spinner = itertools.cycle(['/','-','\\','|','/','-'])
ACTIVATION_TYPES = ['softmax', 'elu', 'selu', 'softplus', 'softsign', 'relu', 'tanh', 'sigmoid', 'hard_sigmoid', 'linear']


def last_value_of(list_object):

    """Method last_value_of() is a helper function to return the last value of an list
    
    	:param list_object: The list which holds a element.
        :type list_object: list

        :returns: The last value of element in list
        :return type: Any
    """

    return list_object[len(list_object) - 1]


def history2String(index, history, cfg):

    """Method history2String() is a helper function to format the history of trained model for individual configuration.
    
    	:param index: The index of individual configuration.
        :type index: int
    	:param history: The history of training.
        :type history: dict
    	:param cfg: The individual configuration.
        :type cfg: json

        :returns: The concatinated and foramtted string of history
        :return type: string
    """

    value = "The run " + str(index) + " has the best result with following configuration:\n" +  str(cfg) + ".\nThe result is:\n"

    value = value + "Accuracy: " + str(last_value_of(history[ACC])) + "\n"

    value = value + "Loss: " + str(last_value_of(history[LOSS])) + "\n"

    value = value + "Validate Accuracy: " + str(last_value_of(history[VAL_ACC])) + "\n"

    value = value + "Validate Loss: " + str(last_value_of(history[VAL_LOSS])) + "\n"

    return  value

def progress():

    """Method progress() is a helper function to visualize the process of a loop

    """
    sys.stdout.write(next(spinner))
    sys.stdout.flush()
    sys.stdout.write('\b')   

def acc_iterator(prediction, acc, error = False):

    """Method acc_iterator() is a helper function to set the userdefined minimum accuracy for the prediction
    
    	:param prediction: The prediction of trained model.
        :type prediction: numpy.array
        :param acc: The minimum accuracy of prediction.
        :type acc: float

        :returns: The prediction with setted accuracy
        :return type: numpy.array
    """

    for p in prediction:

        if p.ndim != 1:

            p = acc_iterator(p, acc, error)

        else:

            if p[0] <= 255 * acc:

                p[0] = 0

            elif error:

                p[0] = 255

    return prediction

def flatten(array):

    """Method flatten() is a helper function to set an array to dimension 1
    
    	:param array: The array which will change.
        :type array: numpy.array

        :returns: The flatten array
        :return type: numpy.array
    """

    flat = array.copy()

    if flat.ndim != 1:

        flat = list(itertools.chain(*flat))

        flat = flatten(np.asarray(flat))

    return flat

def get_model_memory_usage(batch_size, model):
    from keras import backend as K

    shapes_mem_count = 0
    for l in model.layers:
        single_layer_mem = 1
        for s in l.output_shape:
            if s is None:
                continue
            single_layer_mem *= s
        shapes_mem_count += single_layer_mem

    trainable_count = np.sum([K.count_params(p) for p in set(model.trainable_weights)])
    non_trainable_count = np.sum([K.count_params(p) for p in set(model.non_trainable_weights)])

    total_memory = 4.0*batch_size*(shapes_mem_count + trainable_count + non_trainable_count)
    gbytes = np.round(total_memory / (1024.0 ** 3), 3)
    return gbytes

