Classification report
              precision    recall  f1-score   support

not passable       0.99      0.98      0.98   6747528
    passable       0.95      0.96      0.96   2527692

 avg / total       0.98      0.98      0.98   9275220

