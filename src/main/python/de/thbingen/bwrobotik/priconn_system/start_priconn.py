import argparse
import os
import re
import sys
import shutil

sys.path.append('./train')
from PRICoNN import PRICoNN
from Utils import *


ap = argparse.ArgumentParser()
ap.add_argument("-d", "--logdir", default='logs',
                help="path to save logfiles")
ap.add_argument("-c", "--config", nargs='+', required=True, help="list of paths to the configfiles")
ap.add_argument("-w", "--weights", default='init_model/PRICoNN.h5',
	help="Initial weights for CNN")
ap.add_argument("-m", "--model",
	help="Pretrained for CNN")
ap.add_argument("-e", "--eval", default=None,
                help="Configuration for evaluation")
args = vars(ap.parse_args())

if __name__ == "__main__":

    weights = args["weights"]

    model = args["model"]

    list_of_config = args["config"]

    for index in range(0, len(list_of_config)):

        cnn = PRICoNN(list_of_config[index], args["logdir"], weights, model, args["eval"])

        cnn.train()

  
    if os.path.exists(TEMP): 

        shutil.rmtree(TEMP)


