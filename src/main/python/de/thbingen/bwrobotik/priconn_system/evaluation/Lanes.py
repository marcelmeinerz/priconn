"""Lanes module

.. moduleauthor:: Marcel Meinerz <marcel.meinerz@th-bingen.de>


"""
import numpy as np
import cv2
import os
import re
from tqdm import tqdm
import sys

from scipy.misc import imresize
from cv2 import VideoWriter, VideoWriter_fourcc
sys.path.append('./train')
from Utils import *

class Lanes():

    def __init__(self, model, input_shape, accuracy = 0):
        self.recent_fit = []
        self.avg_fit = []
        self.current_model = model
        self.input_shape = input_shape
        self.acc = accuracy/100 

    def predict(self, image, with_acc=True, with_error=False):

        """
        Method to create the prediction of input image.

        :param image: Input image which will be predicted. 
        :type image: numpy.array
        :param with_acc: True if you use the value to set a minimum prediction, otherwise False. 
        :type with_acc: bool
        :param with_error: True if you use the error map function, otherwise False.
        :type with_error: bool

        :returns: The prediction of input image. 
        :return type: numpy.array

        """

        # Get image ready for feeding into model
        small_img = imresize(image, self.input_shape)
        small_img = np.array(small_img)
        small_img = small_img[None,:,:,:]

        # Make prediction with neural network (un-normalize value by multiplying by 255)
        prediction = self.current_model.predict(small_img)[0] * 255

        if with_acc:

            prediction = acc_iterator(prediction, self.acc, with_error)

        return prediction

    def predict_merge(self, image, with_acc=True, with_error=False):

        """
        Method to create the augmented image with green prediction of input image. 

        :param image: Input image which will be predicted. 
        :type image: numpy.array
        :param with_acc: True if you use the value to set a minimum prediction, otherwise False. 
        :type with_acc: bool
        :param with_error: True if you use the error map function, otherwise False.
        :type with_error: bool

        :returns: The augemnted image. 
        :return type: numpy.array

        """

        prediction = self.predict(image, with_acc, with_error)    
        cv2.imwrite(TEMP+'tmp_image.png', prediction)
        prediction = cv2.imread(TEMP+'tmp_image.png')    

        #Set R and B channel to zero, to get an green image for merge
        prediction[:, :, 0] = 0
        prediction[:, :, 2] = 0

        prediction = cv2.resize(prediction, (image.shape[1], image.shape[0]))

        result = cv2.add(image, prediction)

        return result

    def predict_video(self, capture):

        """
        Method to predict images of a video. 

        :param capture: Capture object of video to split it in single images. 
        :type capture: cv2.VideoCapture
        :param shape: Shape of input file. 
        :type shape: tupel

        :returns: The prediction of all images of video. 
        :return type: list (numpy.array)

        """
        
        frames = []
        prediction = []


        sys.stdout.write('Predict video files: ')
        while(capture.isOpened()):

            progress()

            ret, frame = capture.read()
     

            if not ret:
                break

            prediction.append(self.predict_merge(frame, with_acc=False))

        sys.stdout.write('Done\n')
        
        return prediction

    def make_video(self, outvid, size, images=None, fps=30, 
               is_color=True, fourcc=0x7634706d):

        """
        Method to refactore a video from predict_video function. 

        :param outvid: Path to save the video. 
        :type outvid: string
        :param size: Shape of input file. 
        :type size: tupel
        :param images: List of input file. 
        :type images: list (numpy.array)
        :param fps: Parameter to set the frames per second. 
        :type fps: integer
        :param is_color: True if the video use the RGB-channel, otherwise False. 
        :type is_color: bool
        :param fourcc: Hex string of video output type. 
        :type fourcc: Hex string

        :returns: The augemnted video. 
        :return type: VideoWriter

        """
        if int(fps) == 0:
            fps=30


        vid = VideoWriter(outvid, fourcc, fps, size, is_color)

        for img in tqdm(images):
       
            if size[0] != img.shape[1] and size[1] != img.shape[0]:
                
                img = cv2.resize(img, size)

            vid.write(img.astype(np.uint8))

        vid.release()

        return vid

    def error_map(self,label, image):

        """
        Method to create the error map of an image. 

        :param label: Groundtruth of input image. 
        :type label: numpy.array
        :param image: Input image. 
        :type image: numpy.array

        :returns: List of image with colored error, the mask and the augmented input image with the prediction. 
        :return type: list (numpy.array, numpy.array, numpy.array)

        """
     
        #Create road prediction
        prediction = self.predict(image, with_error=True)

        blanks = np.zeros_like(prediction).astype(np.uint8)

        prediction = prediction.flatten()

        #Create label prediction
        label_prediction =label.flatten() * 255

        false_negative = np.zeros_like(prediction).astype(np.uint8)

        false_positive = np.zeros_like(prediction).astype(np.uint8)

        #Evaluate each pixel of image and add it to the specific meaning
        #Positive if prediction and label_prediction are equal
        #False_positive if non passable and passable is predict
        #False_negative if passable and non passable is predict
        for i in range(len(prediction)):

            if (int)(prediction[i]) > (int)(label_prediction[i]):
              
                false_positive[i] = 255

            elif (int)(prediction[i]) < (int)(label_prediction[i]):

                false_negative[i] = 255

        false_negative = np.reshape(false_negative, (self.input_shape[0], self.input_shape[1]))
        false_positive = np.reshape(false_positive, (self.input_shape[0], self.input_shape[1]))

        lane_drawn = np.dstack((false_negative, blanks, false_positive))

        #Re-size to match the original image
        mask = imresize(lane_drawn, image.shape)

        # Merge the lane drawing onto the original image
        error = cv2.add(image, mask)

        # Merge the lane drawing onto the original image
        result = self.predict_merge(image, with_acc=False)

        return (error, mask, result)




