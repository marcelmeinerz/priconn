
import numpy as np
import pickle
import matplotlib.pyplot as plt
import cv2
import os
import re
import sys
import logging
import itertools
import time
import datetime
from tqdm import tqdm

from Lanes import Lanes
sys.path.append('./train')
from Utils import *

from sklearn.utils import shuffle
from sklearn.model_selection import train_test_split
from sklearn.metrics import confusion_matrix, classification_report

"""Eval module

.. moduleauthor:: Marcel Meinerz <marcel.meinerz@th-bingen.de>


"""

class Eval:

    """Class Eval
    """

    def __init__(self, cfg, model):
        
        self.X_val = []
        self.y_val = []
        
        self.param = cfg

        self.eval_map = self.param[ERROR]

        self.model = model

        self.input_shape = (self.param[WIDTH], self.param[HEIGHT], self.param[CHANNEL])


    def __init_components(self, save):

        self.main_folder = save

        if save is None:

            std = datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d_%H:%M')

            self.main_folder = self.param[SAVE]+str(std)

        if not os.path.exists(self.main_folder):

            os.makedirs(self.main_folder)

        outFile = open(self.main_folder+"/evaluation_report.txt", "w")

        if not os.path.exists(TEMP):
            os.makedirs(TEMP)

        return outFile


    def __plot_confusion_matrix(self,cm, classes, save_path='.', normalize=True, title = 'Confusion Matrix', cmap=plt.cm.Blues):

        """Method __plot_confusion_matrix() plot the given confusion_matrix object and save it.

        :param cm: confusion_matrix object. 
        :type cm: sklearn.metrics.confusion_matrix
        :param classes: Classes of confusion matrix and dimension of plot. 
        :type classes: list
        :param save_path: Path to save the plot. Default is lokal path.
        :type save_path: string
        :param normalize: True if the result of each case should be normalize with format {xx.xx%}, otherwise false. Default is True. 
        :type normalize: bool
        :param title: Title of plot and filename to save. Default is Confusion Matrix. 
        :type title: string
        :param cmap: color of type matplotlib.colors.LinearSegmentedColormap object. Default is Blues.
        :type cmap: matplotlib.colors.LinearSegmentedColormap

        """

        try:

            logging.info("Start plot...")

            plt.imshow(cm, interpolation = 'nearest', cmap = cmap)

            plt.title(title)

            plt.colorbar()

            plt.xticks(np.arange(2), classes, rotation = 45)

            plt.yticks(np.arange(2), classes)

            if normalize:

                cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]

            tresh = cm.max() / 2

            for i,j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):

                value = str("%.2f" % round((cm[i, j]* 100), 2) ) + " %"

                plt.text(j, i, value , horizontalalignment = "center", color = "white" if cm[i, j] > tresh else "black")

            plt.tight_layout()

            plt.ylabel("True label")

            plt.xlabel("Predicted label")

            plt.savefig(save_path + '/' + title)

        finally:

            plt.close()

            logging.info("Finish plot...")

    def __create_cm(self, batch_size, outFile):

        """Method __create_cm() calculate the confusion matrix of given files from evaluation JSON file. 
        Additionally save the classification report of calculation.

        :param batch_size: Batch size of model, which will be use for calculation. 
        :type batch_size: int
        :param outFile: File for write down the confusion matrix and classification report. 
        :type outFile: file
 
        """
       
        try:
            
            image = self.X_val[:self.eval_map[MAX_VALUE]]
            label = self.y_val[:self.eval_map[MAX_VALUE]]

            if len(image) != len (label):

                logging.warning("Different count of input files. Given are a length of "+ str(len(image)) + " and " + str(len(label)) + ".")
                logging.warning("Be sure for setting the correct configuration in evaluation JSON file.")
                logging.warning("Creating confusion matrix aborted.")
            
                return

            logging.info("Confusion matrix for "+str(len(image))+" images.")
       
            n_classes = image.shape[0]
            prediction_orig = self.model.predict_classes(image, batch_size, verbose = 0)
            prediction = flatten(prediction_orig)
            label_prediction = flatten(label)
            label_prediction = np.around(label_prediction)

            cm_plot_labels = ["not passable", "passable"]

            report = str(classification_report(list(np.around(label.flatten())), list(prediction_orig.flatten()), target_names = cm_plot_labels))

            outFile.write("Classification report\n"+ report)

            logging.info("Classification report\n"+ report)
       
            cm_o = confusion_matrix(label_prediction, prediction)

            outFile.write(str(cm_o))

            self.__plot_confusion_matrix(cm_o, cm_plot_labels, self.main_folder)

        except:

            logging.error(sys.exc_info())
            logging.error("Be sure for setting the correct configuration in evaluation JSON file.")

        finally:

            del n_classes
            del prediction
            del image
            del label
            del cm_o

    def __error_map(self):

        """Method __error_map() predict the given images and compare them with the given groundtruth from eval JSON file.
        """

        try:

            images = self.X_val[:self.eval_map[MAX_VALUE]]
            labels = self.y_val[:self.eval_map[MAX_VALUE]]

            if len(images) != len (labels):

                logging.warning("Different count of input files. Given are a length of "+ str(len(images)) + " and " + str(len(labels)) + ".")
                logging.warning("Be sure for setting the correct configuration in evaluation JSON file.")
                logging.warning("Image evaluation aborted.")
            
                return

            error_folder = self.main_folder+"/error_map"

            if not os.path.exists(error_folder):
                os.makedirs(error_folder)

            sys.stdout.write('Predict image files: ')

            for index in range(len(images)):

                progress()

                image = images[index]

                image_name = "/eval_"+str(index)+"_"

                label = labels[index]
        
                error, _, result = self.lanes_o.error_map(label, image)

                predict = self.lanes_o.predict(image, False)

                cv2.imwrite(error_folder+image_name+"error.png", error)

                cv2.imwrite(error_folder+image_name+"predict.png", predict)

                cv2.imwrite(error_folder+image_name+"merge.png", result)

            sys.stdout.write('Done\n')

            sys.stdout.flush()

            logging.info("Finish image evaluation...")

        except:

            logging.error(sys.exc_info())
            logging.error("Be sure for setting the correct configuration in evaluation JSON file.")

        finally:

            del images
            del labels

            sys.stdout.flush()
       
    def eval(self, data, save=None):

        """Method eval() start the evaluation of given images and model.

        :param data: Data object, which holds the evaluation images and groundtruth. 
        :type data: Data
        :param save: Save path for results. 
        :type save: string

        """

        logging.info("Run evaluation...")

        outFile = self.__init_components(save)

        input_image = os.path.abspath(self.eval_map[I_PATH]) + '/'

        input_label = os.path.abspath(self.eval_map[L_PATH]) + '/'

        if not data.isGT():

            data.setGroundtruth(self.eval_map[BACKGROUND], self.eval_map[ROAD])

        data.load_eval_data(input_image, input_label, self.param[ARCHIVE])

        self.X_val, self.y_val = data.get_eval_data()

        if len(self.X_val) == 0 or len(self.y_val) == 0:

            logging.warning("No image files for evaluation found. Be sure for setting the correct configuration in evaluation JSON file.")

            logging.warning("Evaluation aborted.")
            
            return

        #Call constructor for evaluation video
        self.lanes_o = Lanes(self.model, self.input_shape, self.param[ACC])

        if self.param[CM]:

            logging.info("Create confusion matrix...")

            self.__create_cm(self.param[BATCH], outFile)

        logging.info("Start image evaluation...")   

        #self.__error_map()

        if len(self.param[V_PATH]) != 0:
 
            logging.info("Start video evaluation...")

        for i in range(len(self.param[V_PATH])):

            logging.info("Create sample video_"+str(i+1)+"...")

            capture = cv2.VideoCapture(os.path.join(self.param[RESOURCE], self.param[V_PATH][i]))

            orig_shape = (int(capture.get(cv2.CAP_PROP_FRAME_WIDTH)), int(capture.get(cv2.CAP_PROP_FRAME_HEIGHT)))

            frame = self.lanes_o.predict_video(capture)

            vid_output = self.main_folder+"/project_video_"+str(i+1)+".mp4"

            self.lanes_o.make_video(vid_output, orig_shape, frame, capture.get(cv2.CAP_PROP_FPS))

        
        logging.info("Finish evaluation")

