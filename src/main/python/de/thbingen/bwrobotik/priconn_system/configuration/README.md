# PRICoNN

## Passable Road Identification with a Convolutional Neural Network

## Configuration

The config.json has following inserts:

### Parameter:

		
		"params" : {
					"width" : 227 ,
					"height" : 227,
					"channel" : 3 ,
					"save_path" : [save_path],
					"activation" : [Activation function],
					"u-net" : false,
					"early_stopping" : false,
					"archive" : true,
					"checkpoint" : {
						"only_the_best" : True,
						"one_4_all" : True
					}
		}
		

* width : Width of input image
* height : Height of input image
* channel : Cannel of input image
* save_path : Path to save the h5py file with trained weights
* activation : The activation function for training. Usually ReLu Function ('relu')
* u-net : True for using of u-net layers, else false
* early_stopping : True if validation accuracy has constant repetition, else false
* archive : True if the input images are as type as Pickle, otherwise false if they are image files
* checkpoint : Settings for checkpoint
	* only_the_best: True to Save only the best weights found in training, otherwise false
	* one_4_all : True to save only one .h5 file for weights, otherwise false. Useful if you are using more then one training

### Optimizer:

	    "Adam" : {
		    "lr" : 0.001,
		    "beta_1" : 0.9,
    		"beta_2" : 0.999,
	    	"epsilon" : 1e-8,
	    	"decay_rate" : 0.0,
		    "amsgrad" : false,
    		"reduce_lr" : true,
    		"reduce_monitor" : "val_loss",
    		"reduce_factor" : 0.2,
    		"reduce_patience" : 2,
    		"reduce_min_lr" : 0.0001,
    		"verbose" : 1
    	},
    	"SGD" : {
    		"lr" : 0.025,
    		"decay_rate" : 5e-4,
    		"momentum" : 0.9,
            "reduce_lr" : true,
    		"reduce_monitor" : "val_loss",
    		"reduce_factor" : 0.2,
    		"reduce_patience" : 2,
    		"reduce_min_lr" : 0.0001,
    		"verbose" : 1
    			
    	},  
    	"DROP" : {
		    "lr" : 0.01,
    		"drop_rate" : 0.5,
    		"epochs" : 10.0,
            "momentum" : 0.8,
    		"verbose" : 1
    	},
    	"TIME" : {
    		"lr" : 0.01,
            "momentum" : 0.8,
    		"verbose" : 1
    	}
    
### Configurations:

		
		"config" : [
					{
					"path_to_image" : "[path_to_image]" ,
					"path_to_label" : "[path_to_label]" ,
					"optimizer" : "Adam",
					"batch_size" : 128 ,
					"epochs" : 10 ,
					"single_data" : false,
					"accuracy" : 20,
					"loss" : mean_squared_error
					},
					{
						...
					}
		]
		

* path_to_image : Path to dataset to image file archive/folder
* path_to_label : Path to dataset to gt_image file archive/folder
* optimizer : Type of optimizer (Adam, DROP, Time, SGD)
* batch_size : Batch size
* epochs : Number of epochs for each train iteration
* single_data : Type of dataset loading, True if only Path to image file is an archive, else False if it is an folder with archives
* accurancy : Minimum accurancy between 0 - 100 for intensity of evaluation video
* loss : Loss function for training

**Note: The "params" and "optimizer" JSON-Objects are for the whole training and the "config" JSON-Object is for the individual training.**
