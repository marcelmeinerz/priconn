.. PRICoNN documentation master file, created by
   sphinx-quickstart on Mon Jul  2 08:41:01 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Optimizer.py
===================================

.. automodule:: Optimizer
   :members:
   :private-members:


