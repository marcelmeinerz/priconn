.. PRICoNN documentation master file, created by
   sphinx-quickstart on Mon Jul  2 08:41:01 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to PRICoNN's documentation!
===================================

.. toctree::
   :maxdepth: 2
   :caption: PRICoNN System Train: 

   PRICoNN
   Data
   Model
   Optimizer

.. toctree::
   :maxdepth: 2
   :caption: PRICoNN System Evaluation:

   Eval
   Lanes

.. toctree::
   :maxdepth: 2
   :caption: PRICoNN Utilities:

   Utils


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
