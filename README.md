# PRICoNN
## Passable Road Identification with a Convolutional Neural Network

The aim of this work is the realization, training and evaluation of a "Fully Convolutional Neural Network" (FCN) for the recognition of roads and paths that can be driven on with colour cameras. The TensorFlow framework is especially discussed and compared with the Python Library keras implemented.

<img src="src/main/resources/show.gif" width="350" height="250">  <img src="src/main/resources/insta.gif" width="350" height="250">

### Prerequisites

You should use Anaconda to create an environment found [here](https://www.anaconda.com/download/#linux).

After download run following code:

`bash ~/Downloads/Anaconda3-[version]-Linux-x86_64.sh`

to install Anaconda. Follow the [instruction](https://docs.anaconda.com/anaconda/install/linux) displayed in terminal.

### Requirements

Use [this conda environment file](priconn.yml). In the command line, use `conda env create -f priconn.yml` and then `source activate priconn` to use the environment.

Alternatively, you can use the following:

The code requires [TensorFlow 1.7](https://www.tensorflow.org/install/), python 3.6 as well as the following python libraries:

* matplotlib
* numpy
* Pillow
* scipy
* ...

Those modules can be installed using: `pip install -r requirement.txt`.


## Tutorial

### Documentation

To use the documentation of this system, you have to install the auto documentation tool [Sphinx](http://www.sphinx-doc.org/en/master/contents.html). To install use

`pip install Sphinx`

and to run the script you have to use

`sphinx-build -b html source/ _build/`

### Getting started

For testing the system you should take the sample files and initial weights stored at [Seafile](https://seafile.bwrobotik.selfhost.de/#my-libs/lib/c25691e2-ac2a-4d62-aad3-26a19562ba2c).
For this you can use the [setup.sh](setup.sh) to autmatically download and store the files.

 `[bash] ./setup.sh`

**Note: The files requires 1 GiB local space an your device or 16 GiB, if you are load the dataset.**

Run `python start_priconn.py -c configuration/config.json` in [sources](src/main/python/de/thbingen/bwrobotik/priconn_system/) to start the learning process of this FCN. The previously downloaded model will be set automatically.

The following options are available and defined as:

* `-c | --config [path to configuration JSON file]`	: Parameter to set a list of configuration JSON files with their path, is required. [more details](src/main/python/de/thbingen/bwrobotik/priconn_system/configuration/README.md)
* `-w | --weights [path to .h5 file]`	            : Parameter to use initial weights, not required
* `-m | --model [path to .h5 file]`	                : Parameter to use pretrained model, not required
* `-e | --eval [path to evaluation JSON file]`      : Parameter to use the evaluation during training, not required. [more details](src/main/python/de/thbingen/bwrobotik/priconn_system/evaluation/README.md)
* `-d | --logdir [log folder]`		                : Parameter to set the folder of log files, default is logs, not required


## Dataset

The Dataset is collected from different sources and you can find it on [seafile](https://seafile.bwrobotik.selfhost.de/#shared-libs/lib/f9245b37-c90b-49ea-bed0-0fca764935f3/PRICoNN_dataset).

The dataset contains:

* [Mapillary Vistas Dataset](https://www.mapillary.com/dataset/vistas?lat=20&lng=0&z=1.5) (need registration)
* [Playing for Data](https://download.visinf.tu-darmstadt.de/data/from_games/)
* [DIPLODOC](https://tev.fbk.eu/sites/tev.fbk.eu/files/roadseqgt.pdf)
* [HyKo](https://ieeexplore.ieee.org/document/8265249/) (not publicly available)
* UGV Mustang (not publicly available)

## Dataset normalization

If you want to use your dataset, you have to normalize your images. To normalize you can use the scripts of [utils](src/main/python/de/thbingen/bwrobotik/utils/) folder.
To use your dataset you have to run [create_data_class.py](src/main/python/de/thbingen/bwrobotik/utils/create_data_class.py) and change the parameter in [data.json](src/main/python/de/thbingen/bwrobotik/utils/data.json).

`python create_data_class.py --data [path to data json file]`

For more details look in [Dataset normalization](src/main/python/de/thbingen/bwrobotik/utils/README.md)

## Evaluation

Run `python start_evaluation.py -e evaluation/eval.json` in [sources](src/main/python/de/thbingen/bwrobotik/priconn_system/) to test the pretrained system. The initial weights will be load automatically.

If you want to evaluate during the training you can run `python start_priconn.py -c configuration/config.json -e evaluation/eval.json`.

The sample files for evaluation are downloaded at the very begin of, by running `./setup.sh`.

For more details look in [Evaluation](src/main/python/de/thbingen/bwrobotik/priconn_system/README.md)

# Citation

If you benefit from this code, please cite my work:

```
@article{meinerz2018priconn,
  title={PRICoNN: Passable Road Identification with a Convolutional Neural Network},
  author={Pauli, Marcel},
  year={2018}
  month = {Aug},
  url = {https://bitbucket.org/marcelmeinerz/priconn/src/master/}
}
```
