#!/bin/bash

RESOURCE=src/main/resources
P_SYSTEM=src/main/python/de/thbingen/bwrobotik/priconn_system

VIDEO_1=$RESOURCE/project_video_field.mp4
VIDEO_2=$RESOURCE/project_video_street.mp4
TEST_DATA=$P_SYSTEM/test_data.tar.gz
DATASET_1=$P_SYSTEM/pd_part1.rar
DATASET_2=$P_SYSTEM/pd_part2.rar
DATASET_3=$P_SYSTEM/pd_part3.rar
DATASET_4=$P_SYSTEM/pd_part4.rar
INIT_WEIGHTS=$P_SYSTEM/init_model/PRICoNN.h5

LINK_VIDEO_1=https://seafile.bwrobotik.selfhost.de:4433/f/7d1bd32bf5/?raw=1 
LINK_VIDEO_2=https://seafile.bwrobotik.selfhost.de:4433/f/455c6b66e7/?raw=1
LINK_DATA_SET_1=https://seafile.bwrobotik.selfhost.de:4433/f/a2de6ff504/?raw=1
LINK_DATA_SET_2=https://seafile.bwrobotik.selfhost.de:4433/f/d939ca2326/?raw=1
LINK_DATA_SET_3=https://seafile.bwrobotik.selfhost.de:4433/f/3ebb36071a/?raw=1
LINK_DATA_SET_4=https://seafile.bwrobotik.selfhost.de:4433/f/c5f1fb4f46/?raw=1
LINK_INIT_WEIGHTS=https://seafile.bwrobotik.selfhost.de:4433/f/bbdc8978aa/?raw=1
LINK_TEST_DATA=https://seafile.bwrobotik.selfhost.de:4433/f/d58d69e369/?raw=1

echo "Start download . . ."

directory_exists_make(){

	if [ -d $1 ]; then
		echo "Directory for $1 exists"
	else
		mkdir $1
	fi

}

download_file(){

	if [ -e $1 ]; then
		echo "File $1 exists"
	else
		wget -O $1 $2
	fi
}

download_and_tar_folder(){

	wget -O $1 $2
	tar -xzvf $1 -C $P_SYSTEM
	rm $1
        echo "Test data successfully downloaded and extract to $P_SYSTEM"

}

extract_to_folder(){

        VALUE=pricon_d_all.rar
        cat $0 $1 $2 $3 > $VALUE
	unrar e $VALUE $P_SYSTEM
        rm $1 $2 $3 $0 $VALUE
        echo "Test data successfully downloaded and extract to $P_SYSTEM/priconn_dataset"

}

echo "Do you want to download the whole training dataset? It use a local space of 16 GB. (not required for testing)"
read -p "Continue (y/N)?"
echo

if [[ $REPLY =~ ^[Yy]$ ]]
then

        if [ $(dpkg-query -W -f='${Status}' unrar 2>/dev/null | grep -c "ok installed") -eq 0 ];
	then
  		apt-get install unrar;
	fi

        echo "Download dataset"
        download_file $DATASET_1 $LINK_DATA_SET_1
        download_file $DATASET_2 $LINK_DATA_SET_2
        download_file $DATASET_3 $LINK_DATA_SET_3
        download_file $DATASET_4 $LINK_DATA_SET_4
 
        extract_to_folder $DATASET_1 $DATASET_2 $DATASET_3 $DATASET_4

else

	echo "Dataset not downloaded. Resume with resource."
	echo "Create directory for test data"
	directory_exists_make $P_SYSTEM/test_data

	download_and_tar_folder $TEST_DATA $LINK_TEST_DATA 

fi

echo "Create directory for resource"
directory_exists_make $RESOURCE

echo "Download sample video files"
download_file $VIDEO_1 $LINK_VIDEO_1

download_file $VIDEO_2 $LINK_VIDEO_2

echo "Create directory for pretrained model"
directory_exists_make $P_SYSTEM/init_model

echo "Download pretrained model"
download_file $INIT_WEIGHTS $LINK_INIT_WEIGHTS

echo "Finish download"
