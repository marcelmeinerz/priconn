\contentsline {lstlisting}{\numberline {1}Parameter Objekt aus Konfiguration}{33}{lstlisting.1}
\contentsline {lstlisting}{\numberline {2}Individuelles Trainings Objekt aus Konfiguration}{33}{lstlisting.2}
\contentsline {lstlisting}{\numberline {3}Optimizer Objekt aus Konfiguration}{34}{lstlisting.3}
\contentsline {lstlisting}{\numberline {4}Objekt aus Evaluierungskonfiguration}{35}{lstlisting.4}
\contentsline {lstlisting}{\numberline {5}Init Model}{36}{lstlisting.5}
\contentsline {lstlisting}{\numberline {6}Init Optimizer}{37}{lstlisting.6}
\contentsline {lstlisting}{\numberline {7}Singleton Pattern in Python}{38}{lstlisting.7}
\contentsline {lstlisting}{\numberline {8}Initialisierung des Model-Objektes}{39}{lstlisting.8}
\contentsline {lstlisting}{\numberline {9}Initialisierung der Aktivierungsfunktion}{39}{lstlisting.9}
\contentsline {lstlisting}{\numberline {10}PRICoNN Model: Encoder}{40}{lstlisting.10}
\contentsline {lstlisting}{\numberline {11}PRICoNN Model: Decoder}{41}{lstlisting.11}
\contentsline {lstlisting}{\numberline {12}Laden der Trainingsdaten}{42}{lstlisting.12}
\contentsline {lstlisting}{\numberline {13}Laden der Evaluationsdaten}{43}{lstlisting.13}
\contentsline {lstlisting}{\numberline {14}Laden des Optimizers}{43}{lstlisting.14}
\contentsline {lstlisting}{\numberline {15}Erstellen des Optimizers}{44}{lstlisting.15}
\contentsline {lstlisting}{\numberline {16}Methode zur Erstellung der Konfusionsmatrix}{45}{lstlisting.16}
\contentsline {lstlisting}{\numberline {17}Methode zur Visualisierung der Fehler}{46}{lstlisting.17}
\contentsline {lstlisting}{\numberline {18}Methode zur Generierung der Vorhersage}{47}{lstlisting.18}
\contentsline {lstlisting}{\numberline {19}Methode zur Bearbeitung der Videos}{47}{lstlisting.19}
\contentsline {lstlisting}{\numberline {20}Image-Manipulation.}{52}{lstlisting.20}
\contentsline {lstlisting}{\numberline {21}Image-Manipulation.}{53}{lstlisting.21}
\contentsline {lstlisting}{\numberline {22}Cropping}{54}{lstlisting.22}
\contentsline {lstlisting}{\numberline {23}Image Normalisierung.}{56}{lstlisting.23}
\contentsline {lstlisting}{\numberline {24}Groundtruth Normalisierung.}{56}{lstlisting.24}
\contentsline {lstlisting}{\numberline {25}Ausgabe der archivierten Dateien.}{57}{lstlisting.25}
