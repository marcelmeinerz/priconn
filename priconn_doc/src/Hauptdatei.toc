\contentsline {section}{\numberline {I}Abk\IeC {\"u}rzungsverzeichnis}{V}{section.1}
\contentsline {section}{\numberline {II}Zusammenfassung}{VI}{section.2}
\contentsline {section}{\numberline {III}Aufbau dieser Arbeit}{VIII}{section.3}
\contentsline {section}{\numberline {1}Einleitung}{1}{section.1}
\contentsline {subsection}{\numberline {1.1}Ausgangssituation}{1}{subsection.1.1}
\contentsline {subsubsection}{\numberline {1.1.1}Motivation}{1}{subsubsection.1.1.1}
\contentsline {subsubsection}{\numberline {1.1.2}Problemstellung}{1}{subsubsection.1.1.2}
\contentsline {subsubsection}{\numberline {1.1.3}Zielsetzung}{2}{subsubsection.1.1.3}
\contentsline {subsection}{\numberline {1.2}Einf\IeC {\"u}hrung ins Bundesamt}{2}{subsection.1.2}
\contentsline {subsubsection}{\numberline {1.2.1}BAAINBw}{2}{subsubsection.1.2.1}
\contentsline {subsubsection}{\numberline {1.2.2}Abteilung U6.2}{3}{subsubsection.1.2.2}
\contentsline {section}{\numberline {2}Grundlagen}{4}{section.2}
\contentsline {subsection}{\numberline {2.1}K\IeC {\"u}nstliche Neuronale Netze}{4}{subsection.2.1}
\contentsline {subsubsection}{\numberline {2.1.1}Convolutional Neural Network}{4}{subsubsection.2.1.1}
\contentsline {subsubsection}{\numberline {2.1.2}Fully Convolutional Neural Network}{5}{subsubsection.2.1.2}
\contentsline {subsubsection}{\numberline {2.1.3}Deconvolutional Neural Network}{6}{subsubsection.2.1.3}
\contentsline {subsubsection}{\numberline {2.1.4}Optimizer}{6}{subsubsection.2.1.4}
\contentsline {paragraph}{\nonumberline Stochastic Gradient Descent}{6}{section*.11}
\contentsline {paragraph}{\nonumberline SGD mit Drop Rate}{7}{section*.12}
\contentsline {paragraph}{\nonumberline SGD mit Zeitlicher Rate}{7}{section*.13}
\contentsline {paragraph}{\nonumberline Adam}{7}{section*.14}
\contentsline {subsubsection}{\numberline {2.1.5}Aktivierungsfunktion}{7}{subsubsection.2.1.5}
\contentsline {paragraph}{\nonumberline Rectifier Linear Unit}{8}{section*.15}
\contentsline {paragraph}{\nonumberline LeakyReLU}{8}{section*.16}
\contentsline {subsubsection}{\numberline {2.1.6}Zielfunktion}{8}{subsubsection.2.1.6}
\contentsline {paragraph}{\nonumberline Binary Cross Entropy}{9}{section*.17}
\contentsline {paragraph}{\nonumberline Mean Absolute Error}{9}{section*.18}
\contentsline {paragraph}{\nonumberline Mean Squared Error}{9}{section*.19}
\contentsline {subsection}{\numberline {2.2}KNN Bibliotheken}{10}{subsection.2.2}
\contentsline {subsubsection}{\numberline {2.2.1}TensorFlow}{10}{subsubsection.2.2.1}
\contentsline {subsubsection}{\numberline {2.2.2}Keras}{10}{subsubsection.2.2.2}
\contentsline {subsubsection}{\numberline {2.2.3}OpenCV}{10}{subsubsection.2.2.3}
\contentsline {subsubsection}{\numberline {2.2.4}TensorBoard}{10}{subsubsection.2.2.4}
\contentsline {subsection}{\numberline {2.3}Evaluierungstechniken}{11}{subsection.2.3}
\contentsline {subsubsection}{\numberline {2.3.1}Konfusionsmatrix}{11}{subsubsection.2.3.1}
\contentsline {subsubsection}{\numberline {2.3.2}Fehlerbild}{12}{subsubsection.2.3.2}
\contentsline {subsubsection}{\numberline {2.3.3}Max F1-Score}{13}{subsubsection.2.3.3}
\contentsline {section}{\numberline {3}Stand der Technik}{14}{section.3}
\contentsline {subsection}{\numberline {3.1}Machine Learning Engineer Nanodegree Capstone Project}{14}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}Kitti-Segmentation}{15}{subsection.3.2}
\contentsline {subsection}{\numberline {3.3}FCN for Semantic Segmentation}{15}{subsection.3.3}
\contentsline {section}{\numberline {4}Eigener Ansatz PRICoNN}{17}{section.4}
\contentsline {subsection}{\numberline {4.1}Hardware Umgebung}{17}{subsection.4.1}
\contentsline {subsection}{\numberline {4.2}Software Umgebung}{17}{subsection.4.2}
\contentsline {subsection}{\numberline {4.3}KNN Architekturen}{18}{subsection.4.3}
\contentsline {subsubsection}{\numberline {4.3.1}FCN8}{18}{subsubsection.4.3.1}
\contentsline {subsubsection}{\numberline {4.3.2}FCN AlexNet}{19}{subsubsection.4.3.2}
\contentsline {subsubsection}{\numberline {4.3.3}U-Net}{19}{subsubsection.4.3.3}
\contentsline {subsection}{\numberline {4.4}Verwendete Architektur}{21}{subsection.4.4}
\contentsline {subsection}{\numberline {4.5}Entwurf PRICoNN}{22}{subsection.4.5}
\contentsline {subsubsection}{\numberline {4.5.1}Normalization Layer}{22}{subsubsection.4.5.1}
\contentsline {subsubsection}{\numberline {4.5.2}Convolutional Layer}{22}{subsubsection.4.5.2}
\contentsline {subsubsection}{\numberline {4.5.3}Pooling Layer}{23}{subsubsection.4.5.3}
\contentsline {subsubsection}{\numberline {4.5.4}Deconvolutional Layer}{24}{subsubsection.4.5.4}
\contentsline {subsubsection}{\numberline {4.5.5}Upsample Layer}{25}{subsubsection.4.5.5}
\contentsline {subsubsection}{\numberline {4.5.6}Aufbau PRICoNN}{26}{subsubsection.4.5.6}
\contentsline {subsubsection}{\numberline {4.5.7}Grenzen von PRICoNN}{29}{subsubsection.4.5.7}
\contentsline {paragraph}{\nonumberline Datensatz}{29}{section*.44}
\contentsline {paragraph}{\nonumberline Modell}{31}{section*.47}
\contentsline {section}{\numberline {5}Implementierung PRICoNN}{32}{section.5}
\contentsline {subsection}{\numberline {5.1}JSON Konfiguration}{32}{subsection.5.1}
\contentsline {paragraph}{\nonumberline Trainingseinstellung}{32}{section*.48}
\contentsline {paragraph}{\nonumberline Evaluierungseinstellung}{35}{section*.49}
\contentsline {subsection}{\numberline {5.2}Klasse PRICoNN}{36}{subsection.5.2}
\contentsline {subsection}{\numberline {5.3}Klasse Model}{38}{subsection.5.3}
\contentsline {subsection}{\numberline {5.4}Klasse Data}{42}{subsection.5.4}
\contentsline {subsection}{\numberline {5.5}Klasse Optimizer}{43}{subsection.5.5}
\contentsline {subsection}{\numberline {5.6}Klasse Eval}{45}{subsection.5.6}
\contentsline {subsection}{\numberline {5.7}Klasse Lanes}{46}{subsection.5.7}
\contentsline {section}{\numberline {6}Trainings- und Evaluationsdaten}{49}{section.6}
\contentsline {subsection}{\numberline {6.1}Vorhandene Datens\IeC {\"a}tze}{49}{subsection.6.1}
\contentsline {subsection}{\numberline {6.2}Spezialisierter Stra\IeC {\ss }en- und Wege-Datensatz}{50}{subsection.6.2}
\contentsline {subsection}{\numberline {6.3}Definition der Befahrbarkeit}{51}{subsection.6.3}
\contentsline {subsection}{\numberline {6.4}Data Augmentation}{51}{subsection.6.4}
\contentsline {subsubsection}{\numberline {6.4.1}Image-Manipulation}{51}{subsubsection.6.4.1}
\contentsline {subsubsection}{\numberline {6.4.2}Cropping}{54}{subsubsection.6.4.2}
\contentsline {subsubsection}{\numberline {6.4.3}Flipping}{55}{subsubsection.6.4.3}
\contentsline {subsection}{\numberline {6.5}Groundtruth}{55}{subsection.6.5}
\contentsline {section}{\numberline {7}Training PRICoNN}{58}{section.7}
\contentsline {subsection}{\numberline {7.1}Parameter}{58}{subsection.7.1}
\contentsline {subsubsection}{\numberline {7.1.1}Model Parameter}{58}{subsubsection.7.1.1}
\contentsline {paragraph}{\nonumberline Accuracy}{58}{section*.60}
\contentsline {paragraph}{\nonumberline Loss}{59}{section*.61}
\contentsline {subsubsection}{\numberline {7.1.2}Optimizer Parameter}{59}{subsubsection.7.1.2}
\contentsline {paragraph}{\nonumberline Learning Rate}{59}{section*.62}
\contentsline {paragraph}{\nonumberline Momentum}{59}{section*.63}
\contentsline {paragraph}{\nonumberline Epsilon}{60}{section*.65}
\contentsline {subsection}{\numberline {7.2}Metriken}{61}{subsection.7.2}
\contentsline {subsubsection}{\numberline {7.2.1}Checkpoint}{61}{subsubsection.7.2.1}
\contentsline {subsubsection}{\numberline {7.2.2}TensorBoard}{61}{subsubsection.7.2.2}
\contentsline {subsection}{\numberline {7.3}Trainingskonfiguration}{62}{subsection.7.3}
\contentsline {subsubsection}{\numberline {7.3.1}Versuchsreihe Adam}{62}{subsubsection.7.3.1}
\contentsline {subsubsection}{\numberline {7.3.2}Versuchsreihe SGD}{63}{subsubsection.7.3.2}
\contentsline {subsection}{\numberline {7.4}Trainingsergebnis}{63}{subsection.7.4}
\contentsline {section}{\numberline {8}Evaluierung PRICoNN}{65}{section.8}
\contentsline {section}{\numberline {9}Ergebnis}{69}{section.9}
\contentsline {section}{\numberline {10}Ausblick}{70}{section.10}
\contentsline {section}{IV. Literaturverzeichnis}{X}{section.10}
\contentsline {section}{V. Abbildungsverzeichnis}{XVI}{section*.77}
\contentsline {section}{VI. Tabellenverzeichnis}{XVIII}{section*.78}
\contentsline {section}{VII. Source Code Verzeichnis}{XIX}{section*.79}
\setcounter {tocdepth}{-1}
\contentsline {section}{\numberline {VIII}Anhang}{XX}{section.8}
\contentsline {paragraph}{\nonumberline Organigramm BAAINBw}{XXIII}{section*.87}
\contentsline {paragraph}{\nonumberline Ausschnitt aus Tensorboard}{XXIV}{section*.89}
\contentsline {paragraph}{\nonumberline U-Net: Biomedical Image Segmentation}{XXV}{section*.91}
\contentsline {paragraph}{\nonumberline PRICoNN Architektur}{XXVI}{section*.93}
\contentsline {paragraph}{\nonumberline Schematischer Ablauf Encoder}{XXVII}{section*.95}
\contentsline {paragraph}{\nonumberline Schematischer Ablauf Decoder}{XXVIII}{section*.97}
\contentsline {paragraph}{\nonumberline Ausschnitt aus Graph des PRICoNN System}{XXIX}{section*.99}
\setcounter {tocdepth}{2}
\contentsline {section}{VIII. Anhang}{XXXVII}{section*.101}
\contentsline {section}{\numberline {IX}Eidesstattliche Erkl\IeC {\"a}rung}{XXXVIII}{section.9}
\contentsline {section}{\numberline {X}Erkl\IeC {\"a}rung zu Eigentum und Urheberrecht}{XXXIX}{section.10}
