
\providecommand{\e}[1]{\ensuremath{\times 10^{#1}}}
\newcolumntype{C}[1]{>{\centering}p{#1}}
\section{Training PRICoNN}\label{train}

In diesem Kapitel werden die Trainingsdaten des finalen CNN erläutern. Anschließend wird beschrieben welche Bedeutung die Metriken haben und welche Parameter sowie Auslastung das Netz bzw. das System hat.
	\subsection{Parameter}
	Die Trainingsparameter sind essentiell und haben großen Einfluss auf das Ergebnis. Daher war es erforderlich diese vorab zu testen und die damit erzielten Ergebnisse auszuwerten. Um diese Parameter nicht ständig im \textit{Source Code} anzupassen, wurden diese zentral in einem JSON Dokument festgehalten. Die Parameter werden in diesem Kapitel genauer erläutert.
		\subsubsection{Model Parameter}
		    Zu jedem Modell gehören Parameter, die eine Form zur Aussage, wie zum Beispiel die Genauigkeit des Trainings oder der Validierung, darstellen. Diese Parameter werden nun im folgenden erklärt. 
			\paragraph{Accuracy}\mbox{} \\
			Der \textit{Accuracy} Parameter gibt die Genauigkeit der Vorhersage gegenüber der \textit{Groundtruth} an. Zur Ermittlung der Genauigkeit verwendet das \textit{keras Framework} standardmäßig den \textit{Categorical Accuracy} Algorithmus [Formel \ref{eq:cat}]. Dieser ermittelt den Mittelwert aus der Summe der Pixel $N$ der Pixel-Gegenüberstellung zwischen Vorhersage und \textit{Groundtruth} der wahrscheinlichsten Klasse\footnote{Die Klasse dessen Wert die größte Übereinstimmung mit der Groundtruth hat. Dazu wird die Funktion argmax verwendet.}. Dabei sind $\hat{y}_i$ wieder die richtige Klassifizierung der Befahrbarkeit oder nicht Befahrbarkeit und ${y_i}$ die \textit{Groundtruth}.
			\begin{equation}\label{eq:cat}
				Categorical\ Accuracy = 1 - \frac{1}{N}\sum_{i=1}^N (argmax(\hat{y}_i) - argmax({y_i}))
			\end{equation}
			Für diese Arbeit wird durch die binäre Klassifizierung jedoch nur der \textit{Binary Accuracy} Algorithmus [Formel \ref{eq:bin}] benötigt. 
			Dieser berechnet die Genauigkeit im Grunde genauso wie der \textit{Categorical Accuracy} Algorithmus mit dem Unterschied, dass die wahrscheinlichste Klasse nicht mehr ermittelt werden muss. Somit wird das Netz im Training performanter.
			\begin{equation}\label{eq:bin}
				Binary\ Accuracy = 1 - \frac{1}{N}\sum_{i=1}^N (\hat{y}_i - {y_i})
			\end{equation}
			\paragraph{Loss}\mbox{} \\
			Der Ausgabeparameter \textit{Loss} ist ein Einzelwert, der beim Training des Modells nach jeder Epoche ausgegeben wird und den Verlust, also das berechnete Ergebnis der zugrundeliegenden Zielfunktion aus \textbf{Kapitel 2.1.6 Zielfunktion}, nach jeder Iteration darstellt. Dieser sollte möglichst klein werden, denn je geringer der Verlust, desto näher sind die Vorhersagen an der \textit{Groundtruth}.
		\subsubsection{Optimizer Parameter}
		    Für das Training und die Anpassung der Gewichte werden \textit{Optimizer} benötigt, wie in \textbf{Kapitel 2.1.4 Optimizer} beschrieben. Diese benötigen die dafür vorgesehenen Parameter, die in diesem Kapitel erläutert werden, um die Gewichte effizient zu bearbeiten. 
			\paragraph{Learning Rate}\mbox{} \\
			Die Lernrate beeinflusst das Training und gibt die Schritte zur Optimierung an. Dabei muss man bei Festlegung einer Lernrate unterschiedliche Szenarien betrachten.
			Wenn die Lernrate niedrig ist, ist das Training zuverlässiger, aber die Optimierung wird viel Zeit in Anspruch nehmen, da die Schritte zum Minimum der Verlustfunktion winzig sind.
			Wenn die Lernrate hoch ist, kann es vorkommen, dass das Training nicht konvergiert oder sogar divergiert. Gewichtsveränderungen können so groß sein, dass der Optimierer das Minimum überschreitet und den Verlust verschlimmert.\\
            Viele \textit{Optimizer} verfügen daher über einen \textit{Scheduler}, der während des Trainings die Lernrate anpasst und somit den Trainingsprozess dynamischer gestaltet. Für dieses Projekt wurde in Anbetracht der geschilderten Fälle für den \textit{Adam-Optimizer} eine Lernrate von 1\e{-3} gewählt mit der dynamischen Lernraten- Anpassung mit dem Faktor $0,5$ und für den \textit{SGD-Optimizer} einen Lernrate von 2.5\e{-3}  mit einer dynamischen Lernraten- Anpassung mit dem Faktor $0,2$ gewählt. Diese werden in der Literatur vorgeschlagen und ergaben auch während der eigenen Experimente die besten Resultate.
			\paragraph{Momentum}\mbox{} \\
			Das Momentum ist ein Impuls, der das Training stabiler machen soll. Ziel eines jeden \textit{Optimizers} ist es, das Ergebnis der Zielfunktion möglichst gering zu bekommen und damit ein globales Minimum zu erreichen. Üblicherweise sind bei der Darstellung dieser Ergebnisse zu meist viele lokale Minima, die dafür sorgen könnten, dass bei geringer Lernrate dieses als globales Minimum gedeutet wird und damit das Training verlangsamt oder zu falschen Ergebnissen führt[Abb \ref{fig:momentum}].  
			\begin{figure}[htb]
				\centering
				\includegraphics[width=0.7\textwidth,angle=0]{abb/momentum}
				\caption[Darstellung lokales/globales Minimum]{Darstellung lokales/globales Minimum}\cite{Momentum}
				\label{fig:momentum}
			\end{figure}
			
			\noindent
			Auch hier muss bei der Wahl des optimalen Momentums auf die Größe des Wertes geachtet werden.	Um diese Situation des falschen globalen Minimums zu vermeiden, verwendet man in der Zielfunktion ein Momentum zwischen 0 und 1, der die Größe der Schritte in Richtung Minimum erhöht, indem er versucht, von einem lokalen Minimum zu springen. Wenn das Momentum groß ist, sollte die Lernrate kleiner gehalten werden. Ein großer Wert des Momentums bedeutet auch, dass die Konvergenz schnell erfolgen wird. Aber wenn sowohl das Momentum als auch die Lernrate auf großen Werten gehalten werden, dann können sie das Minimum mit einem großen Schritt überspringen. Ein kleines Momentum kann lokale Minima nicht zuverlässig vermeiden und das Training des Systems verlangsamen. Momentum hilft auch bei der Glättung der Variationen, wenn die Steigung immer wieder die Richtung ändert. Das Momentum wird für den \textit{SGD-Optimizer} verwendet und wurde für diese Arbeit auf $0,9$ gesetzt. Gemäß der Erläuterung ist das Momentum damit recht groß und die Lernrate im Gegenzug klein.
			
			\paragraph{Epsilon}\mbox{} \\
			Der Epsilon Parameter ist Teil des \textit{Adam Optimizers}. Dieser Wert verhindert eine Division durch Null während der Aktualisierung der Gewichte. Das bedeutet, dass dieser Wert für eine stabile Ermittlung der Genauigkeit nahe der Eins liegen sollte, damit die berechneten Werte nicht zu groß werden können und zu einer Trägheit des Trainings führen. Andererseits wird der \textit{Optimizer} bei kleinem Epsilon instabil. In der Literatur wird von einem standardmäßigen Epsilon von 1\e{-3} ausgegangen. Für diese Arbeit ergab sich ein idealer Wert von 1\e{-8}. Zwar kann der \textit{Optimizer} durch den kleinen Wert instabil werden. Jedoch müssen die für dieses Netz essentiellen Normalisierungsschichten betrachtet werden, die im Falle eines großen Epsilons, die großen Gewichte immer wieder auf Eins normalisieren, was das Netz zwar generalisieren  aber auch stark verfälschen würde. Durch die Verwendung des kleinen Epsilons können große Gewichte dadurch besser verwaltet werden.
	
	\subsection{Metriken}
	Metriken dienen der Auswertung von Daten. Solche Metriken werden auch in Neuronalen Netzen verwendet um diese bei Bedarf anzupassen oder zu verbessern. Die folgenden Metriken wurden speziell für diese Aufgabenstellung ausgesucht und zur Erfüllung des Ziels angepasst.
	    \subsubsection{Checkpoint}
	    Die Metrik \textit{Checkpoint} hat die Aufgabe Ergebnisse nach Bedarf des Benutzers zwischenzuspeichern. Das \textit{keras} eigene Objekt {\fontfamily{pcr}\selectfont ModelCheckpoint} kann dabei so angepasst werden, dass es nur das beste Ergebnis speichert, also spätere Ergebnisse, die schlechter sind als das gespeicherte, ignoriert oder nur das letzte Ergebnis speichert. Zudem funktionieren diese Einstellungen auch beim Unterbrechen und Fortführen des Trainings bei Nutzung der Evaluationsfunktion. Das heißt, dass für den Fall, dass nur das beste Ergebnis gespeichert wird, dieser Wert vor dem Weiterführen des Trainings gesetzt wird und im Prozess verwendet wird.
		\subsubsection{TensorBoard}
		TensorBoard ist, wie in \textbf{Kapitel 2.2.4 TensorBoard} beschrieben, ein Visualisierungs Tool zur Auswertung verschiedener Modell Parameter. Hier kann der Verlauf der Genauigkeit [Abb \ref{fig:tenstrain}] sowie des berechneten Fehlers von Epoche zu Epoche beobachtet werden. Dadurch kann ermittelt werden, dass nach einer gewissen Anzahl von Epochen, der Lernerfolg immer weiter stagniert und die letzten Prozente sehr schwer zu erreichen sind. 
		\begin{figure}[htb]
			\centering
			\includegraphics[width=1\textwidth,angle=0]{abb/tenstrain}
			\caption[Darstellung Genauigkeit der Vorhersage]{Darstellung Genauigkeit der Vorhersage \hyperref[fig:tenstrain_anh]{\faSearchPlus}}
			\label{fig:tenstrain}
		\end{figure}
		Neben der Darstellung der Trainingsergebnisse lässt sich auch das Netz selbst mit Hilfe der TensorBord-Erweiterung darstellen. Der Graph [Anhang \hyperref[fig:tensgraph_anh]{Ausschnitt aus dem Graphen}] ist interaktiv und zeigt dem Nutzer wann und in welchem Abschnitt zum Beispiel Aktionen, wie die Berechnung des Fehlers, durchgeführt werden.
		%\begin{figure}[htb]
		%	\centering
		%	\includegraphics[width=0.7\textwidth,angle=0]{abb/tensgraph}
		%	\caption[Ausschnitt aus dem Graphen]{Ausschnitt aus dem Graphen \hyperref[fig:tensgraph_anh]{\faSearchPlus}}
		%	\label{fig:tensgraph}
		%\end{figure}
		\noindent
		TensorBoard ist eine hilfreiche Erweiterung zum Analysieren des implementierten Netzes und Hilfestellung bei der Anpassung von Trainings- und Evaluationsdaten.
    \newpage
	\subsection{Trainingskonfiguration}\label{ergbenis}
	Das Training ist gleichzustellen mit dem menschlichen Lernen und auch hier hat jeder Mensch seine eigenen Methoden sich Inhalte zu merken und sie zu verstehen. In der KI bzw. bei Neuronalen Netzen ist das ganz ähnlich. Nur hier ist es entscheidend welche Zielfunktion und welchen \textit{Optimizer} man verwendet und welche Aktivierungsfunktion eingesetzt wird. In \textbf{Kapitel 2 Grundlagen} wurden diese kurz erläutert und werden in diesem Kapitel eingesetzt und zeigen die unterschiedlichen Ergebnisse und Einflüsse auf das Netz. Dazu werden Versuchsgruppen in Form der beiden \textit{Optimizer} Adam und SGD, welche jeweils die Versuche mit dem Kreuzprodukt der Aktivierungsfunktionen ReLU und LeakyReLU mit den Zielfunktionen \textit{Mean Squared Error} (MSE), \textit{Mean Absolute Error} (MAE) und \textit{Binary Cross Entropy} (BCE) bilden. Dabei wurden der \textit{Max F1-Score} der beiden Fälle befahrbar (P) und nicht befahrbar (NP) verglichen. Zum Testen werden Datenpakete mit Bildern des UGV Mustang verwendet, die zehn Epochen durchlaufen haben. Die Wahl des Testdatensatzes ist durch den relativ geringen urbanen Anteil von befahrbaren Untergründen entstanden, um zu gewährleisten, dass die gewählte Kombination auch auf den geringen ruralen Anteil der Untergründe im endgültigen Trainingsdatensatz anspricht.
		\subsubsection{Versuchsreihe Adam}
		Der Adam \textit{Optimizer}, wie in \textbf{Kapitel 2.1.4 Optimizer} beschrieben, ist der gängigste Algorithmus im Bereich des CNN. Durch seine dynamische Anpassung der Lernrate während des Trainings, werden die Aktualisierungen der Gewichte gegen Ende des Trainings immer feiner und die Genauigkeit erreicht einen höheren Wert. Die Tabelle \ref{tab:adam} zeigt, dass die Nutzung der LeakyReLU Aktivierungsfunktion im Durchschnitt bei der Verwendung der Zielfunktionen besser ist. Das beste Ergebnis wird aber bei der Nutzung der ReLU Aktivierungsfunktion in Kombination mit der Zielfunktion \textit{Mean Squared Error} gezeigt. Diese Kombination wird deshalb als erste von zwei möglichen Einstellungen\footnote{Zwei Einstellungen, weil zwei leistungsstarke Computer für das Training zur Verfügung stehen.} verwendet.
		\begin{table}[h]
			\centering		
			\caption{Versuchsreihe Adam}
			\begin{tabular}{|l|*6{C{1.6cm}|}}\hline
				 &\multicolumn{2}{c|}{MSE} & \multicolumn{2}{c|}{MAE} & \multicolumn{2}{c|}{BCE} \tabularnewline  
				 & NP & P & NP & P& NP & P  \tabularnewline\hline\hline
				 ReLU & \textbf{98\%} & \textbf{95\%} & 98\% & 94\% & 92\% & 85\% \tabularnewline\hline
				 LeakyReLU & 97\% & 93\% & 98\% & 93\% & 97\% & 94\% \tabularnewline\hline	
			\end{tabular}
			\label{tab:adam}
		\end{table}
		\subsubsection{Versuchsreihe SGD}
		Der zweite übliche Algorithmus bei einem Faltungsnetzwerk ist der SGD Algorithmus. Dieser hat zwar keine dynamische Lernraten-Anpassung, dennoch ist er sehr performant in der Berechnung der Gewichte, was bei der Größe des Datensatzes von Nutzen sein kann. Der SGD Algorithmus hat bei den Versuchen gezeigt, dass auch hier die Zielfunktion \textit{Mean Squared Error} die besten Ergebnisse hervorbringt [Tab \ref{tab:sgd}]. 
		\begin{table}[h]
			\centering		
			\caption{Versuchsreihe SGD}
			\begin{tabular}{|l|*6{C{1.6cm}|}}\hline
				&\multicolumn{2}{c|}{MSE} & \multicolumn{2}{c|}{MAE} & \multicolumn{2}{c|}{BCE} \tabularnewline  
				& NP & P & NP & P& NP & P  \tabularnewline\hline\hline
				ReLU & 97\% & 92\% & 97\% & 93\% & 83\% & 11\% \tabularnewline\hline
				LeakyReLU & \textbf{98\%} & \textbf{95\%} & 97\% & 94\% & 86\% & 1\% \tabularnewline\hline	
			\end{tabular}
			\label{tab:sgd}
		\end{table}
	    \noindent
		Diesmal liefert die Aktivierungsfunktion LeakyReLU aber die besseren Ergebnisse. Somit ist die Kombination aus SGD, LeakyReLU und \textit{Mean Squared Error} als zweite mögliche Einstellung festgelegt.
	\subsection{Trainingsergebnis}
	Die Konfigurationen Adam, ReLU, \textit{Mean Squared Error} (KOMBI1) und SGD, LeakyReLU, \textit{Mean Squared Error} (KOMBI2) werden jeweils mit dem für dieses Projekt erstellten Datensatz aus \textbf{Kapitel 6.1 Vorhandener Datensatz} über 1000 Epochen trainiert und in Iterationen von 50 Epochen evaluiert. Der Trainingsverlauf aus Abbildung \ref{fig:tenseval} zeigt, dass der Graph für KOMBI1 gegen Ende hin leicht stagniert aber dennoch einen stetigen Lernerfolg hat. Das Ergebnis für die Trainingsgenauigkeit für KOMBI1 beläuft sich auf 98,73\%. Dazu im Gegensatz zu KOMBI2, dessen Graph bereits früh bei einem Prozentsatz von 96,26\% stehen bleibt und sich nicht weiter entwickelt. Eine Tabelle mit allen Werten befindet sich im \textbf{Anhang Tabelle mit Trainingsergebnissen}.\\
	Ein endgültiges Resultat bzw. der Erfolg des Trainings kann aber erst durch die Evaluierung deutlich gemacht werden, also durch unbekannte Eingabedaten, die keinen Einfluss auf die Gewichte haben.
	\begin{figure}[htb]
		\centering
		\includegraphics[width=1\textwidth,angle=0]{abb/verg}
		\caption[Vergleich]{Validierungsergebnis aus KOMBI1(blau) und KOMBI2(orange)}
		\label{fig:tenseval}
	\end{figure} 
	\noindent
	