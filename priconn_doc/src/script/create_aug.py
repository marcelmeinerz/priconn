def create_augmented_data(image, label):

    current_x_data = []

    current_y_data = []
    
    #Flipping image
    image_flip = cv2.flip(image, 1)
    current_x_data.append (image_flip)
        
    #Crooping image
    current_x_data.extend(crop_image(image, 3))

    #Flipping groundtruth
    label_flip = cv2.flip(label, 1)
    label_flip = label_flip.reshape(image.shape[0],image.shape[1],1)
    current_y_data.append (label_flip)

    #Crooping groundtruth
    current_y_data.extend(crop_image(label, 1))
 
    #Generate rectangle for image & gt image
    vertex = sp_noise(image,0.000001, 75)
    labeld = False
    for pt in vertex:
        labeld = True
        cv2.rectangle(image, pt[0], pt[1],(0,0,255), cv2.FILLED)
        cv2.rectangle(label, pt[0], pt[1],(0,0,0), cv2.FILLED)
    if(labeld):
        image = cv2.resize(image, size) 
        label = cv2.resize(label, size)  
        label = label.reshape(image.shape[0],image.shape[1],1)
        current_x_data.append (image)
        current_y_data.append (label)

    return (current_x_data, current_y_data)


