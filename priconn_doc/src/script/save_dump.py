
def __save_dump_file(self, current_x_data, current_y_data, r_var):
    #Set folders to save image dump files and label dump files
    train_folder, label_folder = self.__mkdir(r_var, True)
    # Save the given list as pickle dump file
    print('Current_X_data shape:', np.array(current_x_data).shape)
    with open(train_folder+'/input_image_'+self.current_dataset[SUFFIX]+str(r_var)+'.p', 'wb') as save_file:
        pickle.dump(np.array(current_x_data), save_file, -1)
    print('Current_Y_data shape:', np.array(current_y_data).shape)
    with open(label_folder+'/gt_image_'+self.current_dataset[SUFFIX]+str(r_var)+'.p', 'wb') as save_file:
        pickle.dump(np.array(current_y_data), save_file, -1)


def __save_file(self, current_x_data, current_y_data, r_var):
    # Set folders to save image files and label files
    train_folder, label_folder = self.__mkdir(r_var)
    for i in range(len(current_x_data)):
        image_name = uuid.uuid4().hex + ".png"
        while os.path.exists(train_folder+image_name):
            image_name = uuid.uuid4().hex + ".png"
        with open(train_folder + image_name, 'w') as save_file:
            cv2.imwrite(save_file.name, current_x_data[i])
        with open(label_folder + image_name, 'w') as save_file:
            cv2.imwrite(save_file.name, current_y_data[i])


#Save the file to given package size           
if len(current_x_data) >= int(partition):

    if creator.data[PARAM][ARCHIVE]:
        self.__save_dump_file(current_x_data, current_y_data, r_var)
    else:
        self.__save_file(current_x_data, current_y_data, r_var)
