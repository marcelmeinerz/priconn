def __sp_noise(self, image, prob, scaling_width, scaling_height):
    output_vertex = []
    thres = 1 - prob
    # Iterate for each pixel to find random position for pylon in bottom half of image
    for i in range((int)(image.shape[1] / 4), (int)(image.shape[1] / 4) * 3):
        for j in range((int)(image.shape[0] / 2), image.shape[0]):
            rdn = random.random()
            if rdn < prob and len(output_vertex) < 2:
                top = i
                bottom = i + scaling_height
                left = j
                right = j + scaling_width
                # If and only if the possible pylone fits in image, the random pixel will add to list
                if (scaling_width < i) and (i + scaling_height < image.shape[1]) and (scaling_height < j) and (
                        j + scaling_width < image.shape[0]):
                   output_vertex.append(((left, top), (right, bottom)))
    return output_vertex
