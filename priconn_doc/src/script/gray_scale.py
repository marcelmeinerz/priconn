def __gry_scale(self, image):
    label = image.copy()
    # Change gt definitions to gt color of fcn
    label[np.where((label == self.current_dataset[BACKGROUND]).all(axis=2))] = BACKGROUND_COLOR
    label[np.where((label == self.current_dataset[ROAD]).all(axis=2))] = ROAD_COLOR
    label[np.where((label == self.input_image_path[IMAGE_TO_ADD_COLOR]).all(axis=2))] = BACKGROUND_COLOR
    label = cv2.cvtColor(label, cv2.COLOR_BGR2GRAY)
    return label
