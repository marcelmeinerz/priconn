def __paint_object(self, image, pt, isLabel):
    img = image.copy()
    if isLabel:
        obj = cv2.imread(self.input_image_path[L_PATH])
    else:
        obj = cv2.imread(self.input_image_path[I_PATH])
    obj = cv2.resize(obj, ((int)(image.shape[1] * self.ratio), (int)(image.shape[0] * self.ratio)))
    rows, cols, ch = obj.shape
    roi = img[pt[0][0]:pt[1][0], pt[0][1]:pt[1][1]]
    # Now create a mask of logo and create its inverse mask also
    objgray = cv2.cvtColor(obj, cv2.COLOR_BGR2GRAY)
    ret, mask = cv2.threshold(objgray, 10, 255, cv2.THRESH_BINARY)
    mask_inv = cv2.bitwise_not(mask)
    # Now black-out the area of logo in ROI
    img_bg = cv2.bitwise_and(roi, roi, mask=mask_inv)
    # Take only region of logo from logo image.
    obj_fg = cv2.bitwise_and(obj, obj, mask=mask)
    # Put logo in ROI and modify the main image
    dst = cv2.add(img_bg, obj_fg)
    img[pt[0][0]:pt[0][0] + rows, pt[0][1]:pt[0][1] + cols] = dst
    return img
