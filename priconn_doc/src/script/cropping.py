def __crop_image(self, image, channel):
    shp = image.shape
    x2 = (int)(shp[0] / 2)
    y2 = (int)(shp[1] / 2)
    crop_image = [image[0:x2, 0:y2], image[0:x2, y2:shp[1]], image[x2:shp[1], 0:y2], image[x2:shp[0], y2:shp[1]]]
    crop_image_refactor = []
    for img in crop_image:
        img = cv2.resize(img, self.size)
        img = img.reshape(img.shape[0], img.shape[1], channel)
        crop_image_refactor.append(img)

    return crop_image_refactor

