def predict(self, image, with_acc=True, with_error=False):
    # Get image ready for feeding into model
    small_img = imresize(image, self.input_shape)
    small_img = np.array(small_img)
    small_img = small_img[None,:,:,:]
    # Make prediction with neural network (un-normalize value by multiplying by 255)
    prediction = self.current_model.predict(small_img)[0] * 255

    if with_acc:
        prediction = acc_iterator(prediction, self.acc, with_error)

    return prediction


def predict_merge(self, image, with_acc=True, with_error=False):
    prediction = self.predict(image, with_acc, with_error)        
    prediction = cv2.cvtColor(prediction, cv2.COLOR_GRAY2RGB).astype(np.uint8)
    prediction = cv2.resize(prediction, (image.shape[1], image.shape[0]))
    prediction[:, :, 0] = 0
    prediction[:, :, 2] = 0
    result = cv2.add(image, prediction)
    return result


def predict_video(self, capture, shape):        
    frames = []
    prediction = []
    fps = capture.get(cv2.CAP_PROP_FPS)
    while(capture.isOpened()):
        ret, frame = capture.read()
        if not ret:
            break
        prediction.append(self.predict_merge(frame, with_acc=False))
    return prediction

def error_map(self,label, image):     
    #Create road prediction
    prediction = self.predict(image, with_error=True)
    blanks = np.zeros_like(prediction).astype(np.uint8)
    prediction = prediction.flatten()
    #Create label prediction
    label_prediction =label.flatten() * 255
    true_negative = np.zeros_like(prediction).astype(np.uint8)
    false_negative = np.zeros_like(prediction).astype(np.uint8)
    for i in range(len(prediction)):
        if (int)(prediction[i]) > (int)(label_prediction[i]):             
            false_negative[i] = 255
        elif (int)(prediction[i]) < (int)(label_prediction[i]):
            true_negative[i] = 255
    lane_drawn = np.dstack((true_negative, blanks, false_negative))
    mask = imresize(lane_drawn, image.shape)
    error = cv2.add(image, mask)
    result = self.predict_merge(image, with_acc=False)
    return (error, mask, result)
