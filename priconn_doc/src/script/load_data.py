 def load_eval_data(self, input_image_path, input_label_path, archive):
    if archive:
        self.eval_sample, self.eval_label = self.__load_from_dump_file(input_image_path, input_label_path)
    else:
        self.eval_sample, self.eval_label = self.__load_from_file(input_image_path, input_label_path)

    # Make into arrays as the neural network wants these
    self.eval_sample = np.array(self.eval_sample)
    self.eval_label = np.array(self.eval_label)
    # Normalize labels - training images get normalized to start in the network
    self.eval_label = self.eval_label / 255
    return self.get_eval_data()

def load_train_data(self, input_image_path, input_label_path, archive, is_shuffle):
    train_images = []
    labels = []

    if archive:
        train_images, labels = self.__load_from_dump_file(input_image_path, input_label_path)
    else:
        train_images, labels = self.__load_from_file(input_image_path, input_label_path)

    # Make into arrays as the neural network wants these
    train_images = np.array(train_images)
    labels = np.array(labels)
    # Normalize labels - training images get normalized to start in the network
    labels = labels / 255

    # Shuffle images along with their labels, then split into training/validation sets
    if is_shuffle:
        logging.info("Shuffle data...")
        train_images, labels = shuffle(train_images, labels)

    # Test size may be 10% or 20%
    self.X_train, self.X_val, self.y_train, self.y_val = train_test_split(train_images, labels, test_size=0.2)
    logging.info("Train on " + str(len(self.X_train)) + " images, eval on " + str(len(self.X_val)) + " images.")
    return self.get_train_data()
