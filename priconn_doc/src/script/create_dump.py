list_images = self.__get_image_list()
current_x_data = []
current_y_data = []
for i in tqdm(range(len(list_images))):
    # Load images for input
    myFile = list_images[i]
    myFile_name = myFile.split('/')[len(myFile.split('/')) - 1]
    image = cv2.imread(myFile, 3)
    image = cv2.resize(image, self.size)
    current_x_data.append(image)
    # Load images for groundtruth
    label = cv2.imread(self.current_dataset[L_PATH] + self.current_dataset[DESCR] + myFile_name)
    label = self.__check_shape(label, image_aug)
    label = self.__gry_scale(label)
    label = cv2.resize(label, self.size)
    label = label.reshape(image.shape[0], image.shape[1], 1)
    current_y_data.append(label)
