def __create_cm(self, batch_size, outFile):
    image = self.X_val[:self.eval_map[MAX_VALUE]]
    label = self.y_val[:self.eval_map[MAX_VALUE]]

    if len(image) != len(label):
        return

    n_classes = image.shape[0]
    prediction_orig = self.model.predict_classes(image, batch_size, verbose=0)
    prediction = flatten(prediction_orig)
    label_prediction = flatten(label)
    label_prediction = np.around(label_prediction)
    cm_plot_labels = ["not passable", "passable"]
    report = str(classification_report(list(np.around(label.flatten())), list(prediction_orig.flatten()),
                                       target_names=cm_plot_labels))
    outFile.write("Classification report\n" + report)
    logging.info("Classification report\n" + report)
    cm_o = confusion_matrix(label_prediction, prediction)
    outFile.write(str(cm_o))
    self.__plot_confusion_matrix(cm_o, cm_plot_labels, self.main_folder)

