import numpy as np
import scipy as scp
import scipy.misc
import pickle
import matplotlib.pyplot as plt
import os
import re
from tqdm import tqdm

from PIL import Image
from scipy.misc import imresize

import argparse

ap = argparse.ArgumentParser()
ap.add_argument("-d1", "--dump1", required = True,
	help="path to dump files")
ap.add_argument("-d2", "--dump2", required = True,
	help="path to dump files")
args = vars(ap.parse_args())

train_images = pickle.load(open(args["dump1"], "rb" ))
label_images = pickle.load(open(args["dump2"], "rb" ))

if not os.path.exists('aug_data'):
    os.makedirs('aug_data')

for i in tqdm(range(0, len(train_images))):

    image = train_images[i]
    label = label_images[i]

    img = np.array(image)
    lbl = Image.fromarray(label.reshape(227,227),'L')
    
    scp.misc.imsave("aug_data/"+str(i)+"_train_img.png", img)
    scp.misc.imsave("aug_data/"+str(i)+"_label_img.png", lbl)

