def __init_model(self, model=None, weights=None):
    K.clear_session()
    if model is None:
        #Init foundation model singleton for training
        model_o = Model(self.configs[PARAM][ACTIVATION], self.input_shape)
        model = model_o.clone()
        #Load initial weights for model
        if weights is not None and os.path.exists(weights):
            logging.info("Loaded by weights from "+weights+" ...")
            model.load_weights(weights, True)
            logging.info("Done")
        else:
            logging.warning("You should load the model with initial weights! Without weights the model could prone errors!")
    else:
        model = load_model(model)

    return model
