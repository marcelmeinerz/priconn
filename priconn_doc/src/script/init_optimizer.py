def __init_optimizer(self, model, opt, loss, epochs):
    opt_callbacks = []
    if model.optimizer is not None:
        opt_callbacks = self.factory_o.get_optimizer_callbacks(model.optimizer, epochs)
    else
        optimizer, opt_callbacks = self.factory_o.create_optimizer(opt, epochs)
        model.compile(optimizer=optimizer, loss=loss, metrics= [ACC])
return opt_callbacks
