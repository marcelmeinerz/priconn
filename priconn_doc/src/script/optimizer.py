def create_optimizer(self, key, epochs):
    callbacks = []
    optimizer = None
    opt = self.config[key]
    self.global_learning_rate = opt[LR]
    self.decay_rate = opt[LR] / epochs       
    if key.upper() in "SGD":
        if opt[REDUCE]:
            callbacks.append(ReduceLROnPlateau(monitor=opt[MONITOR], factor=opt[FACTOR], patience=opt[PATIENCE], min_lr=opt[MIN_LR], verbose=opt[VERBOSE]))
        optimizer = SGD(lr=opt[LR], momentum=opt[MOM], decay=opt[D_RATE])
    elif key.upper() in "TIME":
        callbacks.append(LearningRateScheduler(time_decay, verbose = opt[VERBOSE]))
        optimizer = SGD(lr=opt[LR], momentum=opt[MOM], decay=self.decay_rate)
        setattr(optimizer, "TIME", 'sub_name')
    elif key.upper() in "DROP":
        callbacks.append(LearningRateScheduler(drop_decay, verbose = opt[VERBOSE]))
        optimizer = SGD(lr=opt[LR], momentum=opt[MOM], decay=self.decay_rate)
        setattr(optimizer, "DROP", 'sub_name')
    else:
        if opt[REDUCE]:
            callbacks.append(ReduceLROnPlateau(monitor=opt[MONITOR], factor=opt[FACTOR], patience=opt[PATIENCE], min_lr=opt[MIN_LR], verbose=opt[VERBOSE]))
        optimizer = Adam(lr=opt[LR], beta_1=opt[BETA_1], beta_2=opt[BETA_2], epsilon=opt[EPSILON], decay=opt[D_RATE], amsgrad=opt[AMSGRAD])

    return (optimizer, callbacks)

def __serialize(self, instance):
    if instance is None:
        return None
    if hasattr(instance, 'sub_name'):
        return getattr(instance, 'sub_name')
    if hasattr(instance, 'get_config'):
        return instance.__class__.__name__            
    if hasattr(instance, '__name__'):
        return instance.__name__
    else:
        raise ValueError('Cannot serialize', instance)

def get_optimizer_callbacks(self, instance, epochs):
    cbs = []
    opt_type = self.__serialize(instance)
    if opt_type is not None:
        _, cbs_tmp = self.create_optimizer(opt_type, epochs)
        cbs.extend(cbs_tmp)
    K.set_value(instance.lr, self.config[opt_type][LR])
    logging.info("Lernrate is set to " + str(K.get_value(instance.lr)))

    return cbs
