from PIL import Image
from numpy import array
from tqdm import tqdm
import pickle 
import numpy as np
import glob
import random
import cv2
import os
import re
import copy
import argparse

ap = argparse.ArgumentParser()
ap.add_argument("-f", "--folder", default='/media/marcelmeinerz/af74694a-261a-4290-aaa7-b6572afe4565/INTENSO', help="path to save dump files")
ap.add_argument("-a", "--augmented", action='store_true')
ap.add_argument("-s", "--size", default=5000, help="size of dump files")
args = vars(ap.parse_args())

images_dir = '/media/marcelmeinerz/af74694a-261a-4290-aaa7-b6572afe4565/mvd/image_2/'
images_label = '/media/marcelmeinerz/af74694a-261a-4290-aaa7-b6572afe4565/mvd/gt_image_2/'
list_images = [images_dir+f for f in os.listdir(images_dir) if re.search('png|PNG', f)]

#######MVD###########
MVD_images_dir = '/media/marcelmeinerz/af74694a-261a-4290-aaa7-b6572afe4565/mvd/image_2/'
MVD_images_label = '/media/marcelmeinerz/af74694a-261a-4290-aaa7-b6572afe4565/mvd/gt_image_2/'
MVD_list_images = [MVD_images_dir+f for f in os.listdir(MVD_images_dir) if re.search('png|PNG', f)]

#######Uni Koblenz###########
UNI_images_dir = '/media/marcelmeinerz/af74694a-261a-4290-aaa7-b6572afe4565/uni_koblenz/image_2/'
UNI_images_label = '/media/marcelmeinerz/af74694a-261a-4290-aaa7-b6572afe4565/uni_koblenz/gt_image_2/'
UNI_list_images = [UNI_images_dir+f for f in os.listdir(UNI_images_dir) if re.search('png|PNG', f)]

#######DIPLODOC###########
DIPLO_images_dir = '/media/marcelmeinerz/af74694a-261a-4290-aaa7-b6572afe4565/diplodoc/image/'
DIPLO_images_label = '/media/marcelmeinerz/af74694a-261a-4290-aaa7-b6572afe4565/diplodoc/gt_image/'
DIPLO_list_images = [DIPLO_images_dir+f for f in os.listdir(DIPLO_images_dir) if re.search('png|PNG', f)]

#######GAME###########
GAME_images_dir = '/media/marcelmeinerz/af74694a-261a-4290-aaa7-b6572afe4565/game/images/'
GAME_images_label = '/media/marcelmeinerz/af74694a-261a-4290-aaa7-b6572afe4565/game/gt_image/'
GAME_list_images = [GAME_images_dir+f for f in os.listdir(GAME_images_dir) if re.search('png|PNG', f)]

#######CITY###########
CITY_images_dir = '/media/marcelmeinerz/af74694a-261a-4290-aaa7-b6572afe4565/cityscape/image/'
CITY_images_label = '/media/marcelmeinerz/af74694a-261a-4290-aaa7-b6572afe4565/cityscape/gt_image_2/'
CITY_list_images = [CITY_images_dir+f for f in os.listdir(CITY_images_dir) if re.search('png|PNG', f)]


X_data = []
Y_data = []
size = (227, 227)
remain = (len(MVD_list_images)+len(UNI_list_images)+len(DIPLO_list_images)+len(GAME_list_images)+len(CITY_list_images))*2

#Create different file path for each dataset
def create_file_path(path, file_name, file_suffix):

    file_path = ""

    if file_suffix in 'GAME':
        file_path = path+"gt"+file_name
    elif file_suffix in 'CITY':
        file_path = path+"gt"+file_name.split('leftImg8bit.png')[0]+"gtFine_color.png"
    else:
        file_path = path+"gt_"+file_name

    return file_path

def sp_noise(image,prob, scaling):
    
    output_vertex = []
    
    thres = 1 - prob 
    for i in range(image.shape[1]):
        for j in range(image.shape[0]):
            rdn = random.random()
            
            if rdn < prob:
                left = 0;
                right = image.shape[1];
                top = 0;
                bottom = image.shape[0];
                if(scaling < i):
                    left = i - scaling
                if(i+scaling < image.shape[1]):
                    right = i + scaling
                if(scaling < j):
                    top = j - scaling
                if(j+scaling < image.shape[0]):
                    bottom = j + scaling
                                   
                output_vertex.append(((left,top) , (right, bottom)))

    
    return output_vertex

def crop_image(image, channel):

    shp = image.shape
    x2 = (int)(shp[0] / 2)
    y2 = (int)(shp[1] / 2)

    crop_image = [image[x2:shp[1] , 0:y2], image[x2:shp[0] , y2:shp[1]]]

    crop_image_refactor = []

    for img in crop_image:
        img = cv2.resize(img, size)
        img = img.reshape(img.shape[0] ,img.shape[1] ,channel)
        crop_image_refactor.append(img)

    return crop_image_refactor

def create_augmented_data(image, label):

    current_x_data = []

    current_y_data = []
    
    #Flipping image
    image_flip = cv2.flip(image, 1)
    image_flip = cv2.resize(image_flip, size) 
    current_x_data.append (image_flip)
        
    #Crooping image
    current_x_data.extend(crop_image(image, 3))

    #Flipping groundtruth
    label_flip = cv2.flip(label, 1)
    label_flip = cv2.resize(label_flip, size) 
    label_flip = label_flip.reshape(image_flip.shape[0],image_flip.shape[1],1)
    current_y_data.append (label_flip)

    #Crooping groundtruth
    current_y_data.extend(crop_image(label, 1))
 
    #Generate rectangle for image & gt image
    vertex = sp_noise(image,0.000001, 75)
    labeld = False
    for pt in vertex:
        labeld = True
        cv2.rectangle(image, pt[0], pt[1],(0,0,255), cv2.FILLED)
        cv2.rectangle(label, pt[0], pt[1],(0,0,0), cv2.FILLED)
    if(labeld):
        image = cv2.resize(image, size) 
        label = cv2.resize(label, size)  
        label = label.reshape(image.shape[0],image.shape[1],1)
        current_x_data.append (image)
        current_y_data.append (label)

    return (current_x_data, current_y_data)

def create_dump_file(list_images, images_label_path, file_suffix):

    current_x_data = []

    current_y_data = []

    r_var = 0   

    for i in tqdm(range(len(list_images))):

        #Load images
        myFile = list_images[i]
        myFile_name = myFile.split('/')[len(myFile.split('/'))-1]
        image = cv2.imread (myFile,3)
        image_aug = image.copy()
        image = cv2.resize(image, size) 
        current_x_data.append (image)
        
        #Load labels
        label = cv2.imread(create_file_path(images_label_path, myFile_name, file_suffix))
        label[np.where((label==[0,0,255]).all(axis=2))] = [0,0,0]
        label[np.where((label==[255,0,255]).all(axis=2))] = [255,255,255]
        label = cv2.cvtColor(label, cv2.COLOR_BGR2GRAY)
        label_aug = label.copy()
        label = cv2.resize(label, size)
        label = label.reshape(image.shape[0],image.shape[1],1)        
        current_y_data.append (label)
        
        #Start data augmentation
        if args["augmented"]:

            x_d, y_d = create_augmented_data(image_aug, label_aug)
            
            current_x_data.extend(x_d)
            current_y_data.extend(y_d)
        
        #Write down dump files for each 3000 images
        if not i % (int)(args["size"]) and i > 0:
            print('Current_X_data shape:', np.array(current_x_data).shape)
            with open(args["folder"]+'/train/train_'+file_suffix+"_"+str(r_var)+'.p', 'wb') as save_file:
                pickle.dump(np.array(current_x_data), save_file, -1)
            print('Current_Y_data shape:', np.array(current_y_data).shape)
            with open(args["folder"]+'/label/label_'+file_suffix+"_"+str(r_var)+'.p', 'wb') as save_file:
                pickle.dump(np.array(current_y_data), save_file, -1)

            r_var = r_var + 1
            current_x_data = []
            current_y_data = []

    
    print('Current_X_data shape:', np.array(current_x_data).shape)
    with open(args["folder"]+'/train/train_'+file_suffix+"_"+str(r_var)+'.p', 'wb') as save_file:
        pickle.dump(np.array(current_x_data), save_file, -1)
    print('Current_Y_data shape:', np.array(current_y_data).shape)
    with open(args["folder"]+'/label/label_'+file_suffix+"_"+str(r_var)+'.p', 'wb') as save_file:
        pickle.dump(np.array(current_y_data), save_file, -1)

if not os.path.exists(args["folder"]+'/train'):
    os.makedirs(args["folder"]+'/train')
if not os.path.exists(args["folder"]+'/label'):
    os.makedirs(args["folder"]+'/label') 

create_dump_file(MVD_list_images,MVD_images_label, "MVD")
create_dump_file(DIPLO_list_images,DIPLO_images_label, "DIPLO")
create_dump_file(UNI_list_images,UNI_images_label, "UNI")
create_dump_file(CITY_list_images,CITY_images_label, "CITY")
create_dump_file(GAME_list_images,GAME_images_label, "GAME")



